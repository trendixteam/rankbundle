<?php

namespace Trendix\RankBundle\Classes\Criterion;

trait DecimalsPropertyTrait
{
    /**
     * @var integer
     * @ORM\Column(name="decimals", type="integer", options={"default"=0})
     */
    private $decimals;

    /**
     * @return mixed
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * @param mixed $decimals
     * @return DecimalsPropertyTrait
     */
    public function setDecimals($decimals)
    {
        $this->decimals = $decimals;
        return $this;
    }
}