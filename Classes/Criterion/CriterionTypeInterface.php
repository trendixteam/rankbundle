<?php

namespace Trendix\RankBundle\Classes\Criterion;

interface CriterionTypeInterface
{
    /**
     * Esta función debe devolver el peso máximo del criterio
     * @return float
     */
    public function getWeight();

    /**
     * Devuelve la cadena de traducción del nombre del criterio
     * @return string
     */
    public static function getName();

    /**
     * Devuelve la clase AnswerType asociada al tipo de criterio
     * @return string
     */
    public static function getAnswerClass();

    /**
     * Devuelve la clase FormulaType asociada al tipo de criterio
     * @return string
     */
    public function getFormulaClass();

    /**
     * Devuelve un array de configuración
     * @return array
     */
    public function getConfiguration();

    /**
     * Devuelve el tipo de criterion (el valor debe coincidir con el del DiscriminatorMap)
     * @return string
     */
    public function getType();
}