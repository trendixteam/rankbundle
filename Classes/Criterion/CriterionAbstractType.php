<?php

namespace Trendix\RankBundle\Classes\Criterion;

use Doctrine\Common\Collections\ArrayCollection;
use Trendix\RankBundle\Classes\Rank\CriterionFillerInterface;
use Trendix\RankBundle\Entity\Criterion\Criterion;

abstract class CriterionAbstractType
    extends Criterion
    implements CriterionTypeInterface,
               CriterionFillerInterface
{

    public function __construct()
    {
    }

    /**
     * Devuelve el peso total del criterio
     * @return float
     */
    public function getWeight()
    {
        return 0.0;
    }

    /**
     * Devuelve un array con la configuración que debe interpretar el servicio generador de formularios
     * @return array
     */
    public function getConfiguration()
    {
        return [];
    }
}