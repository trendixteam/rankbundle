<?php

namespace Trendix\RankBundle\Classes\Answer;

use Trendix\RankBundle\Entity\Answer\Answer;

abstract class AnswerAbstractType extends Answer implements AnswerTypeInterface
{
    /**
     * Devuelve true o false en función de si la pregunta ha sido respondida (si la answer tiene valor).
     * @return bool
     */
    public function isFilled()
    {
        return false;
    }
}