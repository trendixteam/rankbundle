<?php

namespace Trendix\RankBundle\Classes\Answer;

interface AnswerTypeInterface
{
    public static function getType();

    public static function getFormTypes();

    public static function getFormTypeClass();

    public function isFilled();
}