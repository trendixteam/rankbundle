<?php

namespace Trendix\RankBundle\Classes\InheritAnswer;

use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;

interface InheritAnswerInterface
{
    public function transferAnswer(AnswerTypeInterface $parent, AnswerTypeInterface $child);
}