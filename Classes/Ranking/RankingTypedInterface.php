<?php

namespace Trendix\RankBundle\Classes\Ranking;

use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;

interface RankingTypedInterface
{
    /**
     * El constructur de una clase ranking debe recibir un criterio y una respuesta para calcular su puntuación
     * RankingTypedInterface constructor.
     * @param CriterionTypeInterface $criterion
     * @param AnswerTypeInterface $answer
     */
    public function __construct(CriterionTypeInterface $criterion, AnswerTypeInterface $answer);

    /**
     * Devuelve los puntos obtenidos por la respuesta al criterio
     * @return float
     */
    public function getPoints();

    /**
     * Devuelve los puntos máximos que puede devolver el criterio
     * @return float
     */
    public function getMaxPoints();

    /**
     * Devuelve el mínimo de puntos que puede devolver el criterio
     * @return float
     */
    public function getMinPoints();
}