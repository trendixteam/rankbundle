<?php

namespace Trendix\RankBundle\Classes\Ranking;

use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;

abstract class RankingAbstractType implements RankingTypedInterface
{
    /**
     * @var CriterionTypeInterface
     */
    protected $criterion;

    /**
     * @var AnswerTypeInterface
     */
    protected $answer;

    public function __construct(CriterionTypeInterface $criterion, AnswerTypeInterface $answer)
    {
        $this->criterion = $criterion;
        $this->answer = $answer;
    }

    public function getPoints()
    {
        return 0.0;
    }

    public function getMinPoints()
    {
        return 0.0;
    }

    public function getMaxPoints()
    {
        return $this->criterion->getWeight();
    }
}