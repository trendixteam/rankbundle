<?php

namespace Trendix\RankBundle\Classes\Ranking;

interface RankableInterface
{
    public function expirationDate();

    public function isValid();
}