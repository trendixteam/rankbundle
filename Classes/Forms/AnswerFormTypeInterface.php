<?php

namespace Trendix\RankBundle\Classes\Forms;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;

interface AnswerFormTypeInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options);

    /**
     * Este evento se encarga de leer la configuración del criterio asociado a la respuesta
     * y configurar el form en consecuencia
     *
     * @param FormEvent $event
     * @return mixed
     */
    public function onPresetData(FormEvent $event);
}