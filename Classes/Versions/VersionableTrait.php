<?php

namespace Trendix\RankBundle\Classes\Versions;


use Doctrine\Common\Collections\ArrayCollection;

trait VersionableTrait
{
    /**
     * @return ArrayCollection
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * @param ArrayCollection $versions
     * @return EvaluationForm
     */
    public function setVersions(ArrayCollection $versions)
    {
        $this->versions = $versions;
        return $this;
    }
}