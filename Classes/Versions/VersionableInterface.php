<?php

namespace Trendix\RankBundle\Classes\Versions;

use Doctrine\Common\Collections\ArrayCollection;

interface VersionableInterface
{
    public function getVersions();

    public function setVersions(ArrayCollection $versions);
}