<?php

namespace Trendix\RankBundle\Classes\Rank;

interface CriterionFillerInterface
{
    const FILLER_SUPLIER = 'proveedor';
    const FILLER_BUYER = 'comprador';
    const FILLER_TECNICO = 'tecnico';
}