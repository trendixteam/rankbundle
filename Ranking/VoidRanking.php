<?php

namespace Trendix\RankBundle\Ranking;

use Trendix\RankBundle\Classes\Ranking\RankingAbstractType;

/**
 * Para los criterios que no tienen puntuación se usará esta clase
 * Class VoidRanking
 * @package Trendix\RankBundle\Ranking
 */
class VoidRanking extends RankingAbstractType
{

}