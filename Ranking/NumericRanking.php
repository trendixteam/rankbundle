<?php

namespace Trendix\RankBundle\Ranking;

use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Classes\Ranking\RankingAbstractType;
use Trendix\RankBundle\Entity\Criterion\NumericCriterion;

/**
 * Para los criterios que no tienen puntuación se usará esta clase
 * Class VoidRanking
 * @package Trendix\RankBundle\Ranking
 */
class NumericRanking extends RankingAbstractType
{
    private $min;

    private $max;

    private $value;

    private $weight;

    public function __construct(CriterionTypeInterface $criterion, AnswerTypeInterface $answer)
    {
        parent::__construct($criterion, $answer);

        $this->min = $this->criterion->getMinValue();
        $this->max = $this->criterion->getMaxValue();
        $this->value = $this->answer->getValue();
        $this->weight = $this->criterion->getWeight();
    }

    public function getPoints()
    {
        if (null === $this->value) {
            return 0;
        }

        if ($this->criterion->getSubtype() == NumericCriterion::CRITERION_SUBTYPE_LOWER) {
            return $this->getPointsLower();
        } else {
            return $this->getPointsHigher();
        }
    }

    /**
     * Calcula los puntos de un numerico cuyo valor mejor es el más bajo
     */
    private function getPointsLower()
    {
        // Si el valor es menor o igual que el minimo se devuelve el máximo de puntos
        if ($this->value <= $this->min) {
            return $this->weight;
        }

        // Si el valor es mayor o igual que el máximo se devuelve cero puntos (el mínimo)
        if ($this->value >= $this->max) {
            return 0.0;
        }

        return $this->weight * (($this->max - $this->value) / (abs($this->min) + $this->max));
    }

    /**
     * Calcula los puntos de un numerico cuyo valor mejor es el más alto
     */
    private function getPointsHigher()
    {
        // Si el valor es menor o igual que el minimo se devuelve cero puntos
        if ($this->value <= $this->min) {
            return 0.0;
        }

        // Si el valor es mayor o igual que el máximo se devuelve el máximo de puntos
        if ($this->value >= $this->max) {
            return $this->weight;
        }

        return $this->weight * (($this->value - $this->min) / (abs($this->min) + $this->max));
    }
}