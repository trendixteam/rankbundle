<?php

namespace Trendix\RankBundle\Ranking;

use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Classes\Ranking\RankingAbstractType;

/**
 * Para los criterios que no tienen puntuación se usará esta clase
 * Class VoidRanking
 * @package Trendix\RankBundle\Ranking
 */
class RangeRanking extends RankingAbstractType
{
    private $min;

    private $max;

    private $value;

    private $minWeight;

    private $maxWeight;

    public function __construct(CriterionTypeInterface $criterion, AnswerTypeInterface $answer)
    {
        parent::__construct($criterion, $answer);

        $this->min = $this->criterion->getMinValue();
        $this->max = $this->criterion->getMaxValue();
        $this->value = $this->answer->getValue();
        $this->minWeight = $this->criterion->getMinWeight();
        $this->maxWeight = $this->criterion->getMaxWeight();
    }

    public function getPoints()
    {
        if (null === $this->value) {
            return 0;
        }

        // Si el valor está fuera de rango devolvemos 0 puntos
        if($this->value < $this->minWeight || $this->value > $this->max) {
            $puntos = 0;
        } else {
            $puntos = (($this->maxWeight - $this->minWeight) / ($this->max - $this->min)) * ($this->value - $this->min) + $this->minWeight;
        }

        return $puntos;
    }
}