<?php

namespace Trendix\RankBundle\Ranking;

use Trendix\RankBundle\Classes\Ranking\RankingAbstractType;
use Trendix\RankBundle\Entity\Criterion\Option;

/**
 * Para los criterios que no tienen puntuación se usará esta clase
 * Class VoidRanking
 * @package Trendix\RankBundle\Ranking
 */
class SingleOptionRanking extends RankingAbstractType
{
    public function getPoints()
    {
        // Si no hay opción devolvemos 0
        if (!$this->answer->getOption() instanceof Option) {
            return 0;
        }

        return $this->answer->getOption()->getWeight();
    }
}