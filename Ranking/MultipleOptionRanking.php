<?php

namespace Trendix\RankBundle\Ranking;

use Doctrine\Common\Collections\Collection;
use Trendix\RankBundle\Classes\Ranking\RankingAbstractType;

/**
 * Para los criterios que no tienen puntuación se usará esta clase
 * Class VoidRanking
 * @package Trendix\RankBundle\Ranking
 */
class MultipleOptionRanking extends RankingAbstractType
{
    public function getPoints()
    {
        $points = 0;

        if (!$this->answer->getOptions() instanceof Collection) {
            return $points;
        }

        foreach ($this->answer->getOptions() as $option) {
            $points += $option->getWeight();
        }

        return $points;
    }
}