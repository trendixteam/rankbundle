<?php

namespace Trendix\RankBundle\Subscriber;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class SequencePrefixSubscriber implements \Doctrine\Common\EventSubscriber
{
    protected $prefix = '';
    protected $namespace = 'Trendix\RankBundle\Entity';

    public function getSubscribedEvents()
    {
        return array('loadClassMetadata');
    }

    public function __construct($prefix)
    {
        $this->prefix = (string) $prefix;
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $args)
    {
        $classMetadata = $args->getClassMetadata();
        $namespace = $classMetadata->namespace;

        if (!strstr($namespace, $this->namespace) && $namespace != $this->namespace) {
	        return;
        }

        if ($classMetadata->isInheritanceTypeSingleTable() && !$classMetadata->isRootEntity()) {
            return;
        }

        // Para que no haya recursividad en los prefijos para las secuencias (bug de Doctrine)
        if ($classMetadata->rootEntityName != $classMetadata->getName()) {
        	return;
        }

        if ($classMetadata->isIdGeneratorSequence())
        {
            $newDefinition = $classMetadata->sequenceGeneratorDefinition;
            $newDefinition['sequenceName'] = $this->prefix . $newDefinition['sequenceName'];

            $classMetadata->setSequenceGeneratorDefinition($newDefinition);
            $em = $args->getEntityManager();
            if (isset($classMetadata->idGenerator)) {
                $sequenceGenerator = new \Doctrine\ORM\Id\SequenceGenerator(
                    $em->getConfiguration()->getQuoteStrategy()->getSequenceName(
                        $newDefinition,
                        $classMetadata,
                        $em->getConnection()->getDatabasePlatform()),
                    $newDefinition['allocationSize']
                );
                $classMetadata->setIdGenerator($sequenceGenerator);
            }
        }
    }
}