<?php

namespace Trendix\RankBundle\Form\EventListener;


use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;

class CriterionTypeSubscriber implements EventSubscriberInterface
{
	/**
	 * @var Request
	 */
	private $request;

	/**
	 * @var Form
	 */
	private $parentData;

	public function __construct(Form $parentData) {

		$this->parentData = $parentData;
		$this->request = Request::createFromGlobals();
	}

	/**
     * @return mixed
     */
    public static function getSubscribedEvents()
    {
        return [
        	FormEvents::PRE_SET_DATA => 'onPresetData',
	        FormEvents::PRE_SUBMIT => 'onPreSubmit'
        ];
    }

    public static function selectType($type)
    {
        if (isset(Criterion::getFormTypes()[$type])) {
            return Criterion::getFormTypes()[$type];
        }

        return Criterion::getFormTypes()[Criterion::TYPE_NUMERIC];
    }

    private function getType(FormEvent $event)
    {
    	if ($this->request->isXmlHttpRequest()) {
            return $this->request->request->get('type');
	    }

    	return $event->getForm()->get('type')->getData();
    }

    public function onPresetData(FormEvent $event)
    {
    	if (!$this->request->isXmlHttpRequest()) {
			return;
	    }

        $criterionFormType = self::selectType($this->getType($event));

        $this->parentData->add('criteria', CollectionType::class, [
                'entry_type' => $criterionFormType,
                'allow_add' => true,
                'allow_delete' => true,
                'label' => 'str_criterios',
                'prototype_name' => '__criterion__',
		        'entry_options' => [
		        	'parent_data' => null,
			        'data_class' => Criterion::getClassNameFromAlias($this->getType($event))
		        ]
            ]
        );
    }

	public function onPreSubmit(FormEvent $event) {
		if (!$this->request->isMethod( 'post' ) || null === $event->getData() ) {
			return;
		}

		$data = $event->getData();
		$criterionFormType = self::selectType( $data['type'] );
		$dataClass = Criterion::getClassNameFromAlias($data['type']);

		$form = $event->getForm();
	}
}