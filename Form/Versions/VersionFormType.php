<?php

namespace Trendix\RankBundle\Form\Versions;

use Trendix\AdminBundle\Entity\User;
use Trendix\DirectorioBundle\Entity\Subcategoria;
use Trendix\RankBundle\Entity\EvaluationForm;
use Trendix\RankBundle\Entity\Rank;
use Trendix\RankBundle\Entity\Versions\Version;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VersionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('evaluationForm', 'entity', [
                'class' => Rank::class,
                'attr' => ['class' => 'hide', 'readonly' => true],
            ])
            ->add('author', 'entity', [
                'class' => User::class,
                'choices' => [$options['author']],
                'attr' => ['class' => 'hide', 'readonly' => true],
            ])
            ->add('publishAt')
            ->add('subcategoria', 'entity', [
                'class' => Subcategoria::class,
                'choices' => [$options['subcategory']],
                'attr' => ['class' => 'hide', 'readonly' => true],
            ])
            ->add('sections', 'collection', [
                'entry_type' => SectionVersionFormType::class
            ])
            ->add('criteria', 'collection', [
                'entry_type' => CriterionVersionFormType::class
            ])
            ->add('classificationWeight', 'text', [
                'attr' => [
                    'class' => 'trendixAmount',
                    'data-validation' => 'total_percent'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Version::class,
            'author' => '',
            'subcategory' => ''
        ]);
    }

    public function getName()
    {
        return 'evaluation_form_version';
    }
}
