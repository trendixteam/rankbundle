<?php

namespace Trendix\RankBundle\Form\Versions;

use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Versions\CriterionVersion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\BooleanToStringTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriterionVersionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('criterion', 'entity', ['class' => Criterion::class])
            ->add('visible',    'checkbox', ['required' => false, 'attr' => ['class' => 'hide']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CriterionVersion::class
        ]);
    }

    public function getName()
    {
        return 'criterion_version';
    }
}
