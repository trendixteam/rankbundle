<?php

namespace Trendix\RankBundle\Form\Versions;

use Trendix\RankBundle\Entity\Section;
use Trendix\RankBundle\Entity\Versions\SectionVersion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SectionVersionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weight', 'text', [
                'attr' => [
                    'class' => 'trendixAmount',
                    'data-validation' => 'total_percent'
                ]
            ])
            ->add('visible', 'checkbox', ['required' => false, 'attr' => ['class' => 'hide']] )
            ->add('section', 'entity',['class' => Section::class])
        ;
        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SectionVersion::class
        ]);
    }

    public function getName()
    {
        return 'section_version';
    }
}
