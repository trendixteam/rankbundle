<?php

namespace Trendix\RankBundle\Form\Criterion;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Criterion\FileCriterion;
use Trendix\RankBundle\Form\CriterionAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;

class FileType extends CriterionAbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('limitFiles', TextType::class, [
                'label' => 'evaluations.criterion.limit_files',
                'data' => FileCriterion::DEFAULT_LIMIT_FILE,
            ])
            ->add('weight', TextType::class, ['label' => 'str_peso', 'disabled' => true])
            ->add('_type', HiddenType::class, ['data' => FileCriterion::TYPE_FILE, 'mapped' => false]);

        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
		    'data_class' => FileCriterion::class,
		    'criterionType' => Criterion::TYPE_FILE
	    ));
    }

    public function getName() {
	    return FileCriterion::TYPE_FILE;
    }
}