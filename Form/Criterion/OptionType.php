<?php

namespace Trendix\RankBundle\Form\Criterion;

use Trendix\RankBundle\Entity\Criterion\Option;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OptionType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('title', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                'label' => 'str_titulo',
                'attr' => ['placeholder' => 'str_titulo']
            ])
            ->add('weight', TextType::class, [
                'label' => 'str_peso',
                'attr' => ['placeholder' => 'str_peso', 'class' => 'trendixAmount']
            ])
        ;

        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Option::class
        ));
    }

    public function getName() {
        return 'option_for_criterion';
    }
}