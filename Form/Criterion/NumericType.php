<?php

namespace Trendix\RankBundle\Form\Criterion;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Trendix\AdminBundle\Utility\Numeric;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Criterion\NumericCriterion;
use Trendix\RankBundle\Form\CriterionAbstractType;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class NumericType extends CriterionAbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('subtype', ChoiceType::class, [
            	'label' => 'evaluations.criterion.best_value',
	            'choices' => [
                    'evaluations.criterion.lower' => NumericCriterion::CRITERION_SUBTYPE_LOWER,
                    'evaluations.criterion.higher' => NumericCriterion::CRITERION_SUBTYPE_HIGHER
                ],
                'multiple' => false,
                'attr' => [
                    'class' => 'no-select2'
                ]
            ])
            ->add('min_value', \Symfony\Component\Form\Extension\Core\Type\TextType::class, ['label' => 'str_valor_minimo'])
            ->add('max_value', \Symfony\Component\Form\Extension\Core\Type\TextType::class, ['label' => 'str_valor_maximo'])
            ->add('weight', TextType::class, ['label' => 'str_peso'])
            ->add('decimals', TextType::class, [
                'label' => 'str_decimales_mostrar',
                'data' => Numeric::getDecimalNumber()
            ])

	        ->add('_type', HiddenType::class, ['data' => NumericCriterion::TYPE_NUMERIC, 'mapped' => false])
        ;

        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());
        $builder->get('min_value')->addModelTransformer(new TrendixAmountTransformer());
        $builder->get('max_value')->addModelTransformer(new TrendixAmountTransformer());
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
		    'data_class' => NumericCriterion::class,
		    'criterionType' => Criterion::TYPE_NUMERIC
	    ));
    }

	public function getName() {
		return NumericCriterion::TYPE_NUMERIC;
	}
}