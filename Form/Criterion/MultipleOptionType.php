<?php

namespace Trendix\RankBundle\Form\Criterion;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Criterion\MultipleOptionCriterion;
use Trendix\RankBundle\Entity\Criterion\NumericCriterion;
use Trendix\RankBundle\Form\CriterionAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;

class MultipleOptionType extends CriterionAbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('options', CollectionType::class, [
                'entry_type' => OptionType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ])
            ->add('weight', TextType::class, ['label' => 'str_peso', 'disabled' => true])
            ->add('_type', HiddenType::class, ['data' => MultipleOptionCriterion::TYPE_MULTIPLE_OPTION, 'mapped' => false]);
        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => MultipleOptionCriterion::class,
            'criterionType' => Criterion::TYPE_MULTIPLE_OPTION
        ));
    }

    public function getName() {
        return NumericCriterion::TYPE_MULTIPLE_OPTION;
    }
}