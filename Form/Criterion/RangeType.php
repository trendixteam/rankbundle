<?php

namespace Trendix\RankBundle\Form\Criterion;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Trendix\AdminBundle\Utility\Numeric;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Criterion\RangeCriterion;
use Trendix\RankBundle\Form\CriterionAbstractType;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RangeType extends CriterionAbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('minValue', \Symfony\Component\Form\Extension\Core\Type\TextType::class, ['label' => 'str_valor_minimo'])
            ->add('maxValue', \Symfony\Component\Form\Extension\Core\Type\TextType::class, ['label' => 'str_valor_maximo'])
            ->add('minWeight', \Symfony\Component\Form\Extension\Core\Type\TextType::class, ['label' => 'str_peso_valor_minimo'])
            ->add('maxWeight', \Symfony\Component\Form\Extension\Core\Type\TextType::class, ['label' => 'str_peso_valor_maximo'])
            ->add('decimals', TextType::class, [
                'label' => 'str_decimales_mostrar',
                'data' => Numeric::getDecimalNumber()
            ])
            ->add('weight', TextType::class, ['label' => 'str_peso', 'disabled' => true])
	        ->add('_type', HiddenType::class, ['data' => Criterion::TYPE_RANGE, 'mapped' => false])

        ;

        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());
        $builder->get('minValue')->addModelTransformer(new TrendixAmountTransformer());
        $builder->get('maxValue')->addModelTransformer(new TrendixAmountTransformer());
        $builder->get('minWeight')->addModelTransformer(new TrendixAmountTransformer());
        $builder->get('maxWeight')->addModelTransformer(new TrendixAmountTransformer());
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => RangeCriterion::class,
            'criterionType' => Criterion::TYPE_RANGE
        ));
    }

	public function getName() {
		return Criterion::TYPE_RANGE;
	}
}