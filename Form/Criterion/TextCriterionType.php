<?php

namespace Trendix\RankBundle\Form\Criterion;

use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Criterion\NumericCriterion;
use Trendix\RankBundle\Entity\Criterion\TextCriterion;
use Trendix\RankBundle\Form\CriterionAbstractType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;

class TextCriterionType extends CriterionAbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('limitChars', TextType::class, [
                'label' => 'evaluations.criterion.limit_charaters',
                'data' => TextCriterion::DEFAULT_LIMIT_CHARS
            ])
            ->add('weight', TextType::class, ['label' => 'str_peso', 'disabled' => true])
            ->add('_type', HiddenType::class, ['data' => NumericCriterion::TYPE_TEXT, 'mapped' => false]);
        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
		    'data_class' => TextCriterion::class,
		    'criterionType' => Criterion::TYPE_TEXT
	    ));
    }

    public function getName() {
	    return NumericCriterion::TYPE_TEXT;
    }
}