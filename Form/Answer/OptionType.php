<?php

namespace Trendix\RankBundle\Form\Answer;


use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Trendix\RankBundle\Form\DataTransformer\OptionTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->addModelTransformer(new OptionTransformer($options['em']));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('em');
    }

    public function getName()
    {
        return 'option_answer';
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}