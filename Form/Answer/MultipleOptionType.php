<?php

namespace Trendix\RankBundle\Form\Answer;


use Trendix\RankBundle\Entity\Answer\MultipleOptionAnswer;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MultipleOptionType extends AnswerAbstractType
{
    public function getName()
    {
        return MultipleOptionAnswer::getType();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => MultipleOptionAnswer::class
        ));
    }

    public function onPresetData(FormEvent $event)
    {
        parent::onPresetData($event);
        if ($this->renderForm) {
            $options = [];
            foreach($this->criterion->getOptions() as $option) {
                $options[$option->getTitle()] = $option->getId();
            }
            $widgetOptions = $this->getCommonWidgetOptions();
            $widgetOptions['choices'] = $options;
            $widgetOptions['em'] = $this->em;
            $widgetOptions['multiple'] = true;
            $this->form->add('options', OptionsType::class, $widgetOptions);
        }
    }
}