<?php

namespace Trendix\RankBundle\Form\Answer;


use Trendix\AdminBundle\Utility\Numeric;
use Trendix\RankBundle\Entity\Answer\RangeAnswer;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RangeType extends AnswerAbstractType
{
    public function getName()
    {
        return RangeAnswer::getType();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => RangeAnswer::class
        ));
    }

    public function onPresetData(FormEvent $event)
    {
        parent::onPresetData($event);
        if ($this->renderForm) {
            $form = $event->getForm();
            $widgetOptions = $this->getCommonWidgetOptions();
            $widgetOptions['attr'] = [
                'data_validation' => [
                    'min' => (int) floor($this->criterion->getMinValue()),
                    'max' => (int) ceil($this->criterion->getMaxValue())
                ],
                'class' => 'trendixAmount'
            ];

            $form->add('value', NumberType::class, $widgetOptions);
        }
    }

}