<?php

namespace Trendix\RankBundle\Form\Answer;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Trendix\AdminBundle\Utility\Numeric;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NumberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->addModelTransformer(new TrendixAmountTransformer(null, Numeric::getDecimalNumber()));
    }

    public function getName()
    {
        return 'number_answer';
    }

    public function getParent()
    {
        return TextType::class;
    }

}