<?php

namespace Trendix\RankBundle\Form\Answer;


use Trendix\AdminBundle\Utility\Numeric;
use Trendix\RankBundle\Entity\Answer\NumericAnswer;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NumericType extends AnswerAbstractType
{
    public function getName()
    {
        return NumericAnswer::getType();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => NumericAnswer::class
        ));
    }

    public function onPresetData(FormEvent $event)
    {
        parent::onPresetData($event);

        if ($this->renderForm) {
            $form = $event->getForm();
            $widgetOptions = $this->getCommonWidgetOptions();
            $widgetOptions['attr'] = [
                'data_validation' => implode(', ',[
                    'min' => (int) floor($this->criterion->getMinValue()),
                    'max' => (int) ceil($this->criterion->getMaxValue())
                ]),
                'class' => 'trendixAmount'
            ];

            $form->add('value', NumberType::class, $widgetOptions);
        }
    }
}