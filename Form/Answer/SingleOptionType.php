<?php

namespace Trendix\RankBundle\Form\Answer;


use Trendix\RankBundle\Entity\Answer\SingleOptionAnswer;
use Trendix\RankBundle\Entity\Criterion\Option;
use Trendix\RankBundle\Form\DataTransformer\OptionsTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SingleOptionType extends AnswerAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    public function getName()
    {
        return SingleOptionAnswer::getType();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => SingleOptionAnswer::class
        ));
    }

    public function onPresetData(FormEvent $event)
    {
        parent::onPresetData($event);
        if ($this->renderForm) {
            $options = [];
            foreach($this->criterion->getOptions() as $option) {
                $options[$option->getTitle()] = $option->getId();
            }
            $widgetOptions = $this->getCommonWidgetOptions();
            $widgetOptions['choices'] = $options;
            $widgetOptions['em'] = $this->em;
            $this->form->add('option', OptionType::class, $widgetOptions);
        }
    }
}