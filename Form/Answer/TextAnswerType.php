<?php

namespace Trendix\RankBundle\Form\Answer;


use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Trendix\RankBundle\Entity\Answer\TextAnswer;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TextAnswerType extends AnswerAbstractType
{

    public function getName()
    {
        return TextAnswer::getType();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => TextAnswer::class
        ));
    }

    public function onPresetData(FormEvent $event)
    {
        parent::onPresetData($event);
        if ($this->renderForm) {
            $form = $event->getForm();
            $widgetOptions = $this->getCommonWidgetOptions();
            $widgetOptions['attr'] = ['class' => 'span12'];
            $form->add('value', TextareaType::class, $widgetOptions);
        }
    }
}