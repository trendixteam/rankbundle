<?php

namespace Trendix\RankBundle\Form\Answer;


use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\Form\DataTransformer\HiddenEntityTransformer;
use Trendix\AdminBundle\Form\EntityHiddenType;
use Trendix\AdminBundle\Utility\Debug;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Classes\Forms\AnswerFormTypeInterface;
use Trendix\RankBundle\Classes\Rank\CriterionFillerInterface;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Services\Visibilites;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnswerAbstractType extends AbstractType implements AnswerFormTypeInterface
{
    /**
     * @var CriterionTypeInterface
     */
    protected $criterion;

    /**
     * @var Form
     */
    protected $form;

    /**
     * @var AnswerTypeInterface
     */
    protected $answer;

    /**
     * Indica si se debe o no reenderizar el formulario (depende del usuario y el filler del criterio)
     * @var bool
     */
    protected $renderForm = false;

    /**
     * @var EntityManager
     */
    protected $em;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->em = $options['em'];
        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPresetData']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPresetData']);
        $builder->add('_type', HiddenType::class, ['mapped' => false, 'data' => $this->getName()]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rank_answer';
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('em');
    }

    /**
     * {@inheritdoc}
     * @param FormEvent $event
     * @return mixed|void
     */
    public function onPresetData(FormEvent $event)
    {
        // Prepare Data
        $this->answer = $event->getData();
        $this->form = $event->getForm();
        $this->renderForm = false;

        if ($this->answer instanceof AnswerTypeInterface) {
            $this->criterion = $this->findCriterion($this->answer);
            $this->renderForm = true;
        }

        // Prepare Form
        if ($this->renderForm) {
            $this->form->add('criterion', EntityHiddenType::class, [
                'choices' => [$this->criterion],
                'class' => Criterion::class
            ]);
        }
    }

    /**
     * Devuelve las opciones comunes para el widget de una respuesta
     * @return array
     */
    protected function getCommonWidgetOptions()
    {
        if (null === $this->criterion) {
            return [];
        }

        return [
            'label' => $this->criterion->getNumerationAlias(). ' - '.$this->criterion->getTitle(),
            'label_attr' => ['title' => $this->criterion->getDescription()],
            'required' => false
        ];
    }

    private function findCriterion($answer)
    {
        if ($answer instanceof AnswerTypeInterface) {
            return $answer->getCriterion();
        }

        if (is_array($answer)) {
            $criterion = $this->em->getRepository('TrendixRankBundle:Criterion\Criterion')->find($answer['criterion']);
            return $criterion;
        }

        return null;
    }
}
