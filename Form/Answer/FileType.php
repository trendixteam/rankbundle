<?php

namespace Trendix\RankBundle\Form\Answer;


use Trendix\RankBundle\Entity\Answer\FileAnswer;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileType extends AnswerAbstractType
{

    public function getName()
    {
        return FileAnswer::getType();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'data_class' => FileAnswer::class
        ));
    }

    public function onPresetData(FormEvent $event)
    {
        parent::onPresetData($event);
        if (null !== $this->criterion) {
            $form = $event->getForm();
            $widgetOptions = $this->getCommonWidgetOptions();
            $widgetOptions['attr'] = ['class' => 'span12'];
            $widgetOptions['ruta'] = 'answers'.$this->criterion->getNumerationAlias();
            $widgetOptions['limit'] = $this->criterion->getLimitFiles();
            $form->add('files', 'trendix_files', $widgetOptions);
        }
    }
}