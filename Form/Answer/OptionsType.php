<?php

namespace Trendix\RankBundle\Form\Answer;


use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Trendix\RankBundle\Form\DataTransformer\OptionsTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OptionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $options['multiple'] = true;
        parent::buildForm($builder, $options);
        $builder->addModelTransformer(new OptionsTransformer($options['em']));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(['em']);
    }

    public function getName()
    {
        return 'options_answer';
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}