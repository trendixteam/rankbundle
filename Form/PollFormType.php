<?php

namespace Trendix\RankBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\Form\DataTransformer\StringToDateTimeTransformer;
use Trendix\RankBundle\Classes\Rank\CriterionFillerInterface;
use Trendix\RankBundle\Entity\Answer\Answer;
use Trendix\RankBundle\Entity\Evaluation\EvaluationPoll;
use Trendix\RankBundle\Entity\Evaluation\Poll;
use Trendix\RankBundle\Entity\Rank;
use Trendix\RankBundle\Services\Visibilites;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PollFormType extends AbstractType
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var Rank
     */
    protected $rank;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['user'];
        $this->rank = $options['rank'];

        $builder
            ->add('rank', EntityType::class, [
                'class' => 'Trendix\RankBundle\Entity\Rank',
                'choices' => [$this->rank],
                'attr' => ['class' => 'd-none']
            ])
            ->add('user', EntityType::class, [
                'class' => 'Trendix\AdminBundle\Entity\User',
                'choices' => [$this->user],
                'attr' => ['class' => 'd-none']
            ])
            ->add('answers', PolyCollectionType::class, [
                'allow_add' => false,
                'allow_delete' => false,
                'types' => Answer::getFormTypes(),
                'options' => [
                    'em' => $options['em'],
                ]
            ])
            ->addEventListener(FormEvents::SUBMIT, array($this, 'onSubmit'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\RankBundle\Entity\Poll\Poll',
            'files_container' => true // Esta opción debe indicarse en el formulario padre. Hara que limpie los ficheros temporales de todos los trendix_files
        ));

        $resolver->setRequired(['user', 'rank', 'em']);
        //$resolver->addAllowedTypes('user', User::class);
        $resolver->addAllowedTypes('rank', Rank::class);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'poll';
    }

    public function onSubmit(FormEvent $event)
    {
        $poll = $event->getData();
        $this->userManagement($poll);
    }

    /**
     * @param Poll|null $poll
     * @return bool
     */
    private function userManagement($poll = null)
    {
        if (null === $poll) {
            return false;
        }

        if (null === $poll->getCreatedAt()) {
            $poll->setCreatedAt(new \DateTime());
        }

        $poll->setUpdatedAt(new \DateTime());
    }
}
