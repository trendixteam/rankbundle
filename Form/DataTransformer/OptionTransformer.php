<?php

namespace Trendix\RankBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

class OptionTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    public function transform($option)
    {
        if (null === $option) {
            return 0;
        }

        return $option->getId();
    }

    public function reverseTransform($optionId)
    {
        if (null === $optionId) {
            return null;
        }
        
        $option = $this->manager->getRepository('TrendixRankBundle:Criterion\Option')
            ->find($optionId);

        return $option;
    }
}