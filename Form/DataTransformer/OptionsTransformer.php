<?php

namespace Trendix\RankBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

class OptionsTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    public function transform($options)
    {
        if (null === $options) {
            return [];
        }

        $optionsArray = [];
        foreach ($options as $option) {
            $optionsArray[] = $option->getId();
        }
        return $optionsArray;
    }

    public function reverseTransform($options)
    {
        $newOptions = [];
        foreach ($options as $optionId) {
            $newOptions[$optionId] = $this->manager->getRepository('TrendixRankBundle:Criterion\Option')
                ->find($optionId);
        }

        return $newOptions;
    }
}