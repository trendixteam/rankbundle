<?php

namespace Trendix\RankBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Trendix\AdminBundle\Entity\User;
use Trendix\RankBundle\Entity\Rank;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RankType extends AbstractType
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $this->user = $options['user'];

        $builder
            ->add('sections', CollectionType::class, [
                    'type'			=> SectionType::class,
                    'allow_add'		=> true,
                    'allow_delete'	=> true,
                    'label' => 'str_bloques',
                    'prototype_name' => '__block__',
                    'by_reference' => false,
                    'entry_options' => ['em' => $options['em']]
                ]
            )
            ->addEventListener(FormEvents::SUBMIT, array($this, 'onSubmit'))
        ;

        $builder->add('draft', HiddenType::class, [
            'data' => $options['draft'],
            'empty_data' => $options['draft']
        ]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\RankBundle\Entity\Rank',
            'draft' => true
        ));

        $resolver->setRequired(['user', 'em']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rank';
    }

    public function onSubmit(FormEvent $event)
    {
        $rank = $event->getData();
        $this->userManagement($rank);
    }

    protected function userManagement(Rank $rank = null)
    {
        if (null === $rank) {
            return false;
        }

        if (null === $rank->getCreatedBy()) {
            $rank->setCreatedAt(new \DateTime());
            $rank->setCreatedBy($this->user);
        }

        $rank->setUpdatedAt(new \DateTime());
        $rank->setUpdatedBy($this->user);
    }
}
