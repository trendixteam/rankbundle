<?php

namespace Trendix\RankBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Trendix\AdminBundle\Entity\User;
use Trendix\RankBundle\Classes\Rank\CriterionFillerInterface;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriterionAbstractType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     * @return FormBuilderInterface
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->em = $options['em'];

        $builder
            ->add('title', TextType::class, ['label' => 'str_titulo', 'required' => true, 'attr' => ['class' => 'criterion-title']])
            ->add('description',  TextareaType::class, ['label' => 'str_descripcion', 'attr' => ['class' => 'criterion-description']])
            ->add('weight', TextType::class, ['label' => 'str_peso', 'data' => 0, 'required' => true, 'attr' => ['class' => 'criterion-weight']])
            ->add('position', HiddenType::class)
        ;

        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());
        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPresetData']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPresetData']);
    }

    public function onPresetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();
    }

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => CriterionAbstractType::class,
		));

		$resolver->setRequired(['em']);
	}

    /**
     * @return string
     */
    public function getName()
    {
        return 'rank_criterion';
    }
}
