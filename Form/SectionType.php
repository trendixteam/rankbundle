<?php

namespace Trendix\RankBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trendix\AdminBundle\Form\DataTransformer\TrendixAmountTransformer;

class SectionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'str_titulo', 'attr' =>  ['placeholder' => 'str_titulo', 'data-validation' => 'one_criterion']])
            ->add('description', TextareaType::class, ['label' => 'str_descripcion', 'attr' => [
                'placeholder' => 'str_descripcion',
                'class' => 'span12'
            ]])
            ->add('weight', TextType::class, [
                'label' => 'str_peso',
                'attr' => ['data-validation' => 'total_percent', 'class' => 'trendixAmount']
            ])
            ->add('position', NumberType::class, ['label' => 'str_orden', 'required' => true])
            ->add('criteria', PolyCollectionType::class, [
                'types' => $options['types'],
                'allow_add'		=> true,
                'allow_delete'	=> true,
                'label' => 'str_criterios',
                'prototype_name' => '__criterion__',
                'by_reference' => false,
                'options' => ['em' => $options['em']]
            ])
        ;

        $builder->get('weight')->addModelTransformer(new TrendixAmountTransformer());
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Trendix\RankBundle\Entity\Section',
            'types' => Criterion::getFormTypes()
        ));

        $resolver->setRequired(['em']);

        $resolver->setAllowedTypes('types', ['null', 'array']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rank_block';
    }
}
