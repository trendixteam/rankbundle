<?php

namespace Trendix\RankBundle\Form\Util;

use Symfony\Component\Form\FormTypeInterface;


class FormUtil
{
	private static $map = array(
		'Trendix\RankBundle\Form\PolyCollectionType' => 'trendix_form_polycollection',
		'Symfony\Component\Form\Extension\Core\Type\FormType' => 'form',
		'Symfony\Component\Form\Extension\Core\Type\CheckboxType' => 'checkbox',
		'Symfony\Component\Form\Extension\Core\Type\FileType' => 'file',
		'Symfony\Component\Form\Extension\Core\Type\HiddenType' => 'hidden',
		'Symfony\Component\Form\Extension\Core\Type\NumberType' => 'number',
		'Symfony\Component\Form\Extension\Core\Type\TextType' => 'text',
	);

	/**
	 * @param string|FormTypeInterface $type
	 *
	 * @return string
	 */
	public static function getType($type)
	{
		// Compat for SF 2.8+
		if (self::isFullClassNameRequired() && $type instanceof FormTypeInterface) {
			return get_class($type);
		}

		// BC for SF < 2.8 for internally used types
		if (!self::isFullClassNameRequired() && is_string($type) && isset(self::$map[$type])) {
			return self::$map[$type];
		}

		// BC for SF < 2.8 for custom types
		if (!self::isFullClassNameRequired() && $type instanceof FormTypeInterface) {
            //var_dump('entrooo', $type->getName());
            return $type;
        }

		return $type;
	}

	/**
	 * Compatibility for Symfony 2.8+
	 * Checks whether FQCN is required as type name.
	 *
	 * @return bool
	 */
	public static function isFullClassNameRequired()
	{
		static $fqcnRequired = null;

		if ($fqcnRequired === null) {
			$fqcnRequired = method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix');
		}

		return $fqcnRequired;
	}
}