<?php

namespace Trendix\RankBundle\Controller;

use Trendix\AdminBundle\Controller\TrendixBaseController;
use Trendix\RankBundle\Classes\Rank\CriterionFillerInterface;
use Trendix\RankBundle\Entity\Evaluation\EvaluationPoll;
use Trendix\RankBundle\Entity\Rank;
use Trendix\RankBundle\Form\EvaluationPollFormType;
use Trendix\SubastasBundle\Entity\Proveedor;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PollController
{
    /**
     * @Route("/view/{id}", name="ver_formulario_evaluacion")
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function viewEvaluationForm($id)
    {
        $this->sessionStart();

        $proveedor = $this->em->getRepository('TrendixSubastasBundle:Proveedor')->find($id);
        if (!$proveedor instanceof Proveedor) {
            return $this->throwTrendixNotFoundException();
        }

        $formRepo = $this->em->getRepository('TrendixRankBundle:EvaluationForm');
        $pollRepo = $this->em->getRepository('TrendixRankBundle:Evaluation\EvaluationPoll');

        // Añadimos los datos que recibirán todas las vistas
        $this->addData('moduloHomologacionesActivado', $this->validateModuleIsEnabled());
        $this->addData('proveedor', $proveedor);

        $evaluationPoll = $pollRepo->findLastByUser($proveedor->getUsuario());
        // Si no se encuentra formluario de evaluarion mostramos mensaje de que no hay formulario
        if (!$evaluationPoll instanceof EvaluationPoll) {
            $rank = $formRepo->findCurrent();
            if (!$rank instanceof Rank) {
                return $this->render('@TrendixRank/evaluations/poll/empty.html.twig');
            }
            $evaluationPoll = $this->get('evaluation.poll.generator')->createPollFromEvaluation($rank, $proveedor->getUsuario());
        }

        $ranking = $this->get('evaluation.ranking.calculator')->getArrayRankingFromPoll($evaluationPoll);
        $visibilities = $this->get('evaluation.visibilities')->getVisibilities($this->usuario, $evaluationPoll);

        $filler = CriterionFillerInterface::FILLER_BUYER;
        if ($this->usuario->isProveedor()) {
            $filler = CriterionFillerInterface::FILLER_SUPLIER;
        }

        $this->addData('filler', $filler);
        $this->addData('rank', $evaluationPoll->getRank());
        $this->addData('isOlderVersion', $formRepo->isOlderVersion($evaluationPoll->getRank()));
        $this->addData('sections', $pollRepo->findAllCurrentSections($evaluationPoll)); // Prueba de integrar todas las secciones en un mismo arraycollection
        $this->addData('evaluationPoll', $evaluationPoll);
        $this->addData('visibilities', $visibilities);
        $this->addData('proveedor', $proveedor);
        $this->addData('ranking', $ranking);
        $this->addData('classificationSection', $evaluationPoll->getSupplierClassification());
        $this->addData('canFillEvaluationPoll', $this->get('evaluation.access.manager')->canFillEvaluationPoll($this->usuario, $evaluationPoll));

        return $this->render('TrendixRankBundle:evaluations/poll:view.html.twig', $this->getData());
    }

    /**
     * @Route("/fill/{id}", name="rellenar_formulario_evaluacion")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function pollAction(Request $request, $id)
    {
        $this->sessionStart();

        $currentEvaluation = $this->em->getRepository('TrendixRankBundle:EvaluationForm')->findCurrent();
        $proveedor = $this->em->getRepository('TrendixSubastasBundle:Proveedor')->find($id);
        if (!$proveedor instanceof Proveedor) {
            return $this->throwTrendixNotFoundException();
        }

        $evaluationPoll = $this->get('evaluation.poll.generator')->createPollFromEvaluation($currentEvaluation, $proveedor->getUsuario());
        if (!$this->get('evaluation.access.manager')->canFillEvaluationPoll($this->usuario, $evaluationPoll)) {
            return $this->throwTrendixNotFoundException();
        }

        $sections = $this->em->getRepository('TrendixRankBundle:Evaluation\EvaluationPoll')->findAllCurrentSections($evaluationPoll);
        $visibilities = $this->get('evaluation.visibilities');
        $visibilities->setPoll($evaluationPoll);

        $formOptions = [
            'rank' => $evaluationPoll->getRank(),
            'owner' => $proveedor->getContacto()->getUsuario(),
            'filler' => $this->usuario,
            'em' => $this->em,
            'visibilities' => $visibilities
        ];
        $form = $this->createForm(new EvaluationPollFormType(), $evaluationPoll, $formOptions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($evaluationPoll);
            $this->em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'evaluations.guardado_correctamente');
            return $this->redirectToRoute('ver_formulario_evaluacion', ['id' => $proveedor->getId()]);
        }

        $visibilities = $this->get('evaluation.visibilities')->getVisibilities($this->usuario, $evaluationPoll);

        $filler = CriterionFillerInterface::FILLER_BUYER;
        if ($this->usuario->isProveedor()) {
            $filler = CriterionFillerInterface::FILLER_SUPLIER;
        }

        $this->addData('filler', $filler);
        $this->addData('visibilities', $visibilities);
        $this->addData('form', $form->createView());
        $this->addData('rank', $currentEvaluation);
        $this->addData('sections', $sections);
        $this->addData('proveedor', $proveedor);
        $this->addData('moduloHomologacionesActivado', true);
        $this->addData('canValidate', $this->get('evaluation.access.manager')->canValidateEvaluationPoll($this->usuario, $evaluationPoll));

        return $this->render('TrendixRankBundle:evaluations/poll:Poll.html.twig', $this->getData());
    }
}
