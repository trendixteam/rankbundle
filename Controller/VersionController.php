<?php

namespace Trendix\RankBundle\Controller;

use Trendix\AdminBundle\Controller\TrendixBaseController;
use Trendix\AdminBundle\Utility\Dates;
use Trendix\DirectorioBundle\Entity\Subcategoria;
use Trendix\RankBundle\Entity\Versions\Version;
use Trendix\RankBundle\Form\Versions\VersionFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/evaluation/version")
 * Class VersionController
 * @package Trendix\RankBundle\Controller
 */
class VersionController extends TrendixBaseController
{
    /**
     * @Route("/list", name="list_evaluation_version")
     *
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function listAction()
    {
        $this->sessionStart();
        $this->validateUserType(true, false, false, true);

        $versions = $this->em->getRepository('TrendixRankBundle:Versions\Version')->findAllVersionsByUser($this->usuario);
        $subcategories = $this->em->getRepository('DirectorioBundle:Subcategoria')->findManagedByUser($this->usuario);

        $this->addData('subcategorias', $subcategories);
        $this->addData('versions', $versions);

        return $this->render('@TrendixRank/evaluations/category/list.html.twig', $this->getData());
    }

    /**
     * @Route("/view/{id}", name="view_evaluation_version")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        $this->sessionStart();

        $this->sessionStart();
        $version = $this->em->getRepository('TrendixRankBundle:Versions\Version')->find($id);

        $versionFormOptions = [
            'author' => $version->getAuthor(),
            'subcategory' => $version->getSubcategoria()
        ];

        $form = $this->createForm(new VersionFormType(), $version, $versionFormOptions);

        $this->addData('readonly', true);
        $this->addData('form', $form->createView());
        $this->addData('subcategoria', $version->getSubcategoria());
        $this->addData('evaluationVersion', $version);

        return $this->render('@TrendixRank/evaluations/category/EvaluationForm.html.twig', $this->getData());
    }

    /**
     * @Route("/edit/{id}", name="edit_evaluation_version")
     *
     * @param $id
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction($id)
    {
        $this->sessionStart();
        $version = $this->em->getRepository('TrendixRankBundle:Versions\Version')->find($id);
        $classificationSection = $this->em->getRepository('TrendixRankBundle:SupplierClassification')->findCurrent();

        $versionFormOptions = [
            'author' => $this->usuario,
            'subcategory' => $version->getSubcategoria()
        ];

        $form = $this->createForm(new VersionFormType(), $version, $versionFormOptions);

        $this->addData('form', $form->createView());
        $this->addData('classificationSection', $classificationSection);
        $this->addData('subcategoria', $version->getSubcategoria());
        $this->addData('evaluationVersion', $version);

        return $this->render('@TrendixRank/evaluations/category/EvaluationForm.html.twig', $this->getData());
    }

    /**
     * @Route("/new/{subcategory}", name="new_evaluation_version")
     *
     * @param $subcategory
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function newAction($subcategory)
    {
        $this->sessionStart();

        $subcategory = $this->em->getRepository('DirectorioBundle:Subcategoria')->find($subcategory);
        $currentForm = $this->em->getRepository('TrendixRankBundle:EvaluationForm')->findCurrent();

        $version = new Version($currentForm);
        $version->setSubcategoria($subcategory);
        $version->setAuthor($this->usuario);

        $this->em->persist($version);
        $this->em->flush($version);

        return $this->redirectToRoute('edit_evaluation_version', ['id' => $version->getId()], 302);
    }

    /**
     * @Route("/save/{id}", name="save_evaluation_version")
     *
     * @param $id
     * @param Request $request
     * @return \Trendix\SubastasBundle\Response\JSONResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveAction($id, Request $request)
    {
        $this->sessionStart();
        $trans = $this->get('translator');

        $version = $this->em->getRepository('TrendixRankBundle:Versions\Version')->find($id);
        $versionFormOptions = [
            'author' => $this->usuario,
            'subcategory' => $version->getSubcategoria()
        ];

        $form = $this->createForm(new VersionFormType(), $version, $versionFormOptions);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->returnFailResponse($trans->trans('str_generico_error'), true);
        }

        $version = $form->getData();
        if ($request->request->get('aprobar') == 'true') {
            $version->setPublishAt(Dates::getDateTimeWithMilisecs());
        }

        $this->em->persist($version);
        $this->em->flush();

        return $this->returnSuccessResponse($trans->trans('evaluations.guardado_correctamente'), true);
    }

    /**
     * @Route("/duplicate", name="duplicate_evaluation_version")
     *
     * @param Request $request
     * @return \Trendix\SubastasBundle\Response\JSONResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function duplicateAction(Request $request)
    {
        $this->sessionStart();
        $trans = $this->get('translator');
        $originId = $request->request->get('origin');
        $origin = $this->em->getRepository('TrendixRankBundle:Versions\Version')->find($originId);
        if (!$origin instanceof Version) {
            return $this->returnFailResponse($trans->trans('str_generico_error'), true);
        }

        $subcategories = $request->request->get('subcategories');
        foreach ($subcategories as $s) {
            $subcategory = $this->em->getRepository('DirectorioBundle:Subcategoria')->find($s);
            $this->deleteCurrentCategoryVersion($subcategory);
            $newVer = clone $origin;
            $newVer->setAuthor($this->usuario);
            $newVer->setSubcategoria($subcategory);
            $this->em->persist($newVer);
        }

        $this->em->flush();

        return $this->returnSuccessResponse($trans->trans('evaluations.guardado_correctamente'), true);
    }

    private function deleteCurrentCategoryVersion(Subcategoria $s)
    {
        $version = $this->em->getRepository('TrendixRankBundle:Versions\Version')->findCurrentByCategory($s);
        if (!$version instanceof Version) {
            return false;
        }

        $this->em->remove($version);
    }
}
