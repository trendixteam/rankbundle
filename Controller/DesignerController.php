<?php

namespace Trendix\RankBundle\Controller;

use Doctrine\ORM\ORMException;
use Trendix\AdminBundle\Controller\TrendixBaseController;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Form\EvaluationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Trendix\AdminBundle\Utility\Debug;
/**
 * Class DesignerController
 * @package Trendix\RankBundle\Controller
 *
 * @Route("/evaluationform")
 */
class DesignerController extends TrendixBaseController
{
    /**
     * @Route("/designer/", name="design_evaluation_form")
     */
    public function designerAction(Request $request)
    {
        // Validación de permisos por tipo de usuario
        if (!$this->get('evaluation.access.manager')->canEditEvaluationFromDesign($this->usuario)) {
            return $this->throwTrendixNotFoundException();
        }

        $draft = (bool) $request->get('draft');

        // Obtenemos la última Rank y la clonamos si no es borrador
        $evaluation = $this->em->getRepository('TrendixRankBundle:EvaluationForm')->findLastToDesign();
        $SupplierClassification = $this->em->getRepository('TrendixRankBundle:SupplierClassification')->findCurrent();

        $form = $this->createForm(new EvaluationFormType(), $evaluation, ['user' => $this->usuario, 'draft' => $draft, 'em' => $this->em]);

        try {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $evaluation = $form->getData();
                $this->em->persist($evaluation);
                $this->em->flush();

                // Heredamos las versiones al nuevo Formulario si no es un borrador
                if (!$evaluation->isDraft()) {
                    $this->get('evaluation.inherit.version')->inheritVersions($evaluation);
                }

                $this->get('session')->getFlashBag()->add('notice', 'evaluations.guardado_correctamente');
                // Evitamos el reenvío del formulario
                //return $this->redirect($this->generateUrl('design_evaluation_form'));
            }
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('error', 'evaluations.error_procesando_formulario');
            // Evitamos el reenvío del formulario
            return $this->redirect($this->generateUrl('design_evaluation_form'));
        }

        $this->addData('form', $form->createView());
        $this->addData('criteriaType', Criterion::getTypes());
        $this->addData('criteriaFormType', $this->get('trendix.rank.manager')->createFormWithCriterionTypes(Criterion::getFormTypes()));
        $this->addData('classificationSection', $SupplierClassification);
        $this->addData('rank', $evaluation);

        return $this->render('TrendixRankBundle:evaluations/evaluation:Designer.html.twig', $this->getData());
    }

    /**
     * @param Request $request
     *
     * @Route("/ajax/fillerlist", name="evaluation_update_filler_list")

     * @return \Trendix\SubastasBundle\Response\JSONResponse
     */
    public function AjaxFillerList(Request $request)
    {
        $this->sessionStart();

        $rol = $request->request->get('rol');

        $users = $this->em->getRepository('AdminBundle:User')->getUsersByRolArray($rol);
        $this->addData('userFillers', $users);

        return $this->returnSuccessResponse(null, true);
    }
}
