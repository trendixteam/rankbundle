<?php

namespace Trendix\RankBundle\Controller;

use Trendix\AdminBundle\Controller\TrendixBaseController;
use Trendix\RankBundle\Entity\CriterionType\NumericCriterion;
use Trendix\RankBundle\Entity\Rank;
use Trendix\RankBundle\Form\RankType;
use Trendix\RankBundle\TrendixRankBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends TrendixBaseController
{
    /**
     * @Route("/rank/test/")
     */
    public function indexAction(Request $request)
    {
        $this->sessionStart();

        $rank = new Rank();

        $form = $this->createForm(new RankType(), $rank, ['user' => $this->usuario]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($rank);
            $this->em->flush();

            return $this->redirect($this->generateUrl('ver_rank', ['id' => $rank->getId()]));
        }

        $this->addData('form', $form->createView());

        if ($request->isXmlHttpRequest()) {
            return $this->render('TrendixRankBundle:Forms:CriterionType.html.twig', $this->getData());
        }

        return $this->render('TrendixRankBundle:Forms:rank.html.twig', $this->getData());
    }

    /**
     * @param $id
     *
     * @Route("/rank/ver/{id}", name = "ver_rank")
     */
    public function verRank($id)
    {
    	$this->sessionStart();
        $rank = $this->em->getRepository('TrendixRankBundle:Rank')->find($id);

        var_dump($rank->getVersions()->first()->getBlocks()->first()->getCriteria());
        exit;
    }
}
