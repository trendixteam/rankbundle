<?php

namespace Trendix\RankBundle\InheritAnswer;

use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\InheritAnswer\InheritAnswerInterface;
use Trendix\RankBundle\Entity\Answer\SingleOptionAnswer;

class SingleOptionInheritAnswer implements InheritAnswerInterface
{
    /**
     * @param SingleOptionAnswer $parent
     * @param SingleOptionAnswer $child
     * @return AnswerTypeInterface
     */
    public function transferAnswer(AnswerTypeInterface $parent, AnswerTypeInterface $child)
    {
        $option = $parent->getOption();
        $criterion = $child->getCriterion();
        foreach ($criterion->getOptions() as $o) {
            if ($o->getTitle() == $option->getTitle()) {
                $child->setOption($o);
            }
        }

        return $child;
    }
}