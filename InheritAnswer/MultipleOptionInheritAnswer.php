<?php

namespace Trendix\RankBundle\InheritAnswer;

use Doctrine\Common\Collections\ArrayCollection;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\InheritAnswer\InheritAnswerInterface;

class MultipleOptionInheritAnswer implements InheritAnswerInterface
{
    public function transferAnswer(AnswerTypeInterface $parent, AnswerTypeInterface $child)
    {
        $options = $parent->getOptions();
        $criterion = $child->getCriterion();

        $selectedOptions = new ArrayCollection();

        foreach ($criterion->getOptions() as $o) {
            foreach ($options as $option) {
                if ($o->getTitle() == $option->getTitle()) {
                    $selectedOptions->add($o);
                }
            }
        }

        $child->setOptions($selectedOptions);

        return $child;
    }
}