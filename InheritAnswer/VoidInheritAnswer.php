<?php

namespace Trendix\RankBundle\InheritAnswer;

use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\InheritAnswer\InheritAnswerInterface;

class VoidInheritAnswer implements InheritAnswerInterface
{
    public function transferAnswer(AnswerTypeInterface $parent, AnswerTypeInterface $child)
    {
        return $child;
    }
}