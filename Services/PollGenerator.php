<?php

namespace Trendix\RankBundle\Services;


use Doctrine\ORM\EntityManager;
use Trendix\AdminBundle\Entity\User;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Entity\Answer\Answer;
use Trendix\RankBundle\Entity\Poll\Poll;
use Trendix\RankBundle\Entity\RecurrentForm;
use Trendix\RankBundle\Entity\Rank;
use Symfony\Component\DependencyInjection\Container;

class PollGenerator
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var User
     */
    private $user;

    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * @param Rank $form
     * @param User $user
     * @return Poll
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createPoll(Rank $form, $user, $class = null)
    {
        $this->user = $user;
        // Si se pasa la clase del Poll, se usa esa, si no, se usa Poll directamente:
        if($class) {
            $poll = new $class();
        } else {
            $poll = new Poll();
        }
        $poll->setRank($form)->setUser($user);

        // Añadimos las respuestas para los criterios de evaluacion
        $criteria = $this->em->getRepository('TrendixRankBundle:Rank')->findCriteriaFromRank($form);
        foreach ($criteria as $criterion) {
            $poll->addAnswer($this->container->get('evaluation.inherit.answers')->createAnswer($criterion, $this->user));
        }

        // Y la guardamos
        $this->em->persist($poll);
        $this->em->flush($poll);

        return $poll;
    }

    private function isValid($poll, Rank $form)
    {
        if (!$poll instanceof Poll || $form->getId() != $poll->getRank()->getId()) {
            return false;
        }

        $criteria = $this->em->getRepository('TrendixRankBundle:Rank')->findCriteriaFromRank($form);
        if ($criteria->count() != $poll->getAnswers()->count()) {
            return false;
        }

        return true;
    }
}