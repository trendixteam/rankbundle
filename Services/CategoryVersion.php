<?php

namespace Trendix\RankBundle\Services;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Trendix\AdminBundle\Entity\User;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Entity\Evaluation\EvaluationPoll;
use Trendix\RankBundle\Entity\Section;
use Trendix\RankBundle\Entity\Versions\CriterionVersion;
use Trendix\RankBundle\Entity\Versions\SectionVersion;
use Trendix\RankBundle\Entity\Versions\Version;

class CategoryVersion
{
    /**
     * @var ArrayCollection
     */
    private $versions;

    /**
     * @var ArrayCollection
     */
    private $categories;

    /**
     * @var EvaluationPoll
     */
    private $poll;

    public function __construct(EvaluationPoll $poll)
    {
        $this->poll = $poll;
        $this->getCategories($poll->getUser());
        $this->getVersions();

    }

    /**
     * @return null|Version
     */
    public function fusionVersion()
    {
        if (!$this->poll->getUser()->isProveedor()) {
            return null;
        }

        $fusion = new Version();
        $fusion->setEvaluationPoll($this->poll);

        foreach ($this->poll->getRank()->getSections() as $section) {
            $fusion->addSection($this->fusionSection($section));
            foreach ($section->getCriteria() as $criterion) {
                $fusion->addCriterion($this->fusionCriterion($criterion));
            }
        }

        return $fusion;
    }

    /**
     * Obtiene un listado de las subcategories del proveedor
     * @param User $user
     */
    private function getCategories(User $user)
    {
        if (!$user->isProveedor()) {
            $this->categories = new ArrayCollection();
        }

        $prov = $user->getProveedor();
        $this->categories = $prov->getSubcategoriasAsignadas();
    }

    /**
     * Obtiene las versiones de cada subcategoría del proveedor
     */
    private function getVersions()
    {
        $this->versions = new ArrayCollection();
        foreach ($this->categories as $category) {
            if ($category->getLastVersion() instanceof Version) {
                $this->versions->add($category->getLastVersion());
            } else {
                $this->versions->add($this->getDefaultVersion());
            }
        }
    }

    /**
     * Genera la versión por defecto
     */
    private function getDefaultVersion()
    {
        $form = $this->poll->getRank();
        return new Version($form);
    }

    /**
     * Fusion las versiones de la sección pasada por parámetro
     *
     * @param Section $section
     * @return SectionVersion
     */
    private function fusionSection(Section $section)
    {
        $fsv = new SectionVersion();
        $fsv->setWeight($this->getAverageSectionWeight($section));
        $fsv->setSection($section);

        $visible = false;
        foreach ($this->versions as $v) {
            $sv = $v->findBySection($section);
            $visible = ($visible || $sv->isVisible());
        }

        $fsv->setVisible($visible);

        return $fsv;
    }

    /**
     * @param Section $section
     * @return float|int
     */
    private function getAverageSectionWeight(Section $section)
    {
        $fusionWeight = 0;
        $n = $this->categories->count();
        $sum = $this->getSumWeight($section);

        if ($n > 0) {
            $fusionWeight = $sum / $n;
        }

        return $fusionWeight;
    }

    /**
     * Devuelve la suma de los pesos de las secciones de cada version
     * @param Section $section
     * @return int
     */
    private function getSumWeight(Section $section)
    {
        $weight = 0;
        foreach ($this->versions as $v) {
            $v->getSections();
            $vs = $v->findBySection($section);
            $weight += $vs->getWeight();
        }

        return $weight;
    }

    /**
     * Fusiona las versiones de los criterios de cada version
     *
     * @param CriterionTypeInterface $criterion
     * @return CriterionVersion
     */
    private function fusionCriterion(CriterionTypeInterface $criterion)
    {
        $fcv = new CriterionVersion();
        $fcv->setCriterion($criterion);

        $visible = false;
        foreach ($this->versions as $v) {
            $cv = $v->findByCriterion($criterion);
            $visible = ($visible || $cv->isVisible());
        }

        $fcv->setVisible($visible);

        return $fcv;
    }
}
