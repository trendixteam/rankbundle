<?php

/**
 * El método de calculo de ranking utiliza el "Algoritmo de la mochila"
 * Este se encarga de ir haciendo calculo parciales y almacenarlos para finalmente obtener el resultado deseado
 * Aprovechando este método vamos obteniendo también las puntuaciones de cada respuesta realizada y las de cada
 * sección sin aumentar el coste de tiempo del cálculo
 *
 * @see https://es.wikibooks.org/wiki/Programaci%C3%B3n_din%C3%A1mica/Problema_de_la_mochila
 */

namespace Trendix\RankBundle\Services;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\Utility\Dates;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\Ranking\RankingTypedInterface;
use Trendix\RankBundle\Entity\Answer\Answer;
use Trendix\RankBundle\Entity\Answer\MultipleOptionAnswer;
use Trendix\RankBundle\Entity\Answer\NumericAnswer;
use Trendix\RankBundle\Entity\Answer\RangeAnswer;
use Trendix\RankBundle\Entity\Answer\SingleOptionAnswer;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Criterion\MultipleOptionCriterion;
use Trendix\RankBundle\Entity\Criterion\NumericCriterion;
use Trendix\RankBundle\Entity\Criterion\RangeCriterion;
use Trendix\RankBundle\Entity\Criterion\SingleOptionCriterion;
use Trendix\RankBundle\Entity\Evaluation\Poll;
use Trendix\RankBundle\Entity\Rank;
use Symfony\Component\Translation\TranslatorInterface;
use Trendix\RankBundle\Form\Answer\NumberType;

class Similarity
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TranslatorInterface
     */
    private $trans;

    /**
     * @var Poll
     */
    private $poll;

    /**
     * Indica si se han respondido a todas las preguntas
     * @var boolean
     */
    private $completed;

    public function __construct(EntityManager $em, TranslatorInterface $trans)
    {
        $this->em = $em;
        $this->trans = $trans;
        $this->completed = true;
    }

    /**
     * @param ArrayCollection|array $polls
     * @param Poll $poll
     * @return array
     */
    public function getArrayRankingFromPoll($polls, $poll, $numericThreshold = 0.8, $textThreshold = 10, $selectedThresold = 1)
    {
        $this->completed = true;
        $this->poll = $poll;
        if (null === $this->poll) {
            return $ranking;
        }

        $totalCriteria = 0;
        $totalAnswered = 0;

        // Recabamos los datos y el algortimo a emplear según el tipo de criterio
        $values = [];
        $mode = [];
        foreach ($polls as $poll) {
            /** @var \Trendix\RankBundle\Entity\Poll\Poll $poll */
            foreach ($poll->getAnswers() as $answer) {
                /** @var Answer $answer */
                $value = [];
                $criterionId = $answer->getCriterion()->getId();
                if(isset($values[$criterionId])) {
                    $value = $values[$criterionId];
                }
                list($newMode, $newValue) = $this->evaluateAnswer($answer);
                $value[] = $newValue;
                $values[$criterionId] = $value;
                $mode[$criterionId] = $newMode;
            }
        }

        // Comparamos el poll actual con el resto
        $result = [];
        foreach ($this->poll->getAnswers() as $answer) {
            /** @var Answer $answer */
            $criterionId = $answer->getCriterion()->getId();
            list($mode, $value) = $this->evaluateAnswer($answer);
            if($mode == 'average') {
                if(count($values) > 0) {
                    $average = array_sum($values[$criterionId]) / count($values[$criterionId]);
                    if($average == $value) { // Para evitar divisiones por cero, si ambos valores son iguales, se devuelve 1 sean cuales sean estos valores
                        $comparison = 1;
                    } elseif($average > $value) {
                        $comparison = $value / $average;
                    } else {
                        $comparison = $average / $value;
                    }
                    if($comparison >= $numericThreshold) {
                        $result[$criterionId] = 1;
                    } else {
                        $result[$criterionId] = 0;
                    }
                } else {
                    $result[$criterionId] = 0;
                }
            } elseif($mode == 'selected') {
                // TODO replantear selected
                $counts = array_count_values($values[$criterionId]);

                if(isset($counts[$value]) && $counts[$value] >= $selectedThresold) {
                    $result[$criterionId] = 1;
                } else {
                    $result[$criterionId] = 0;
                }
            } elseif($mode == 'levenshtein') {
                // Para las comparaciones de texto, bastará con que al menos uno de los productos gustados coincida
                $minMatch = 9999;
                foreach ($values[$criterionId] as $val) {
                    $match = levenshtein($value, $val);
                    if($match < $textThreshold && $match < $minMatch) {
                        $minMatch < $minMatch;
                    }
                }
                if($minMatch <= $textThreshold) {
                    $result[$criterionId] = 1;
                } else {
                    $result[$criterionId] = 0;
                }
            }
        }

        // Devolvemos el porcentaje de coincidencia en función de los umbrales y los resultados obtenidos
        $total  = count($result);
        $goodCriteria = array_sum(array_values($result));
        if($total == 0) {
            return 0;
        }
        return $goodCriteria / $total;
    }

    /**
     * @param $answer
     * @return array
     */
    private function evaluateAnswer($answer): array
    {
        if ($answer instanceof NumericAnswer
            || $answer instanceof RangeAnswer) {
            $newMode = 'average';
            $newValue = $answer->getValue();
        } elseif ($answer instanceof SingleOptionAnswer) {
            $newMode = 'selected';
            $newValue = $answer->getOption()->getId();
        } elseif ($answer instanceof MultipleOptionAnswer) {
            $newMode = 'selected';
            $options = [];
            foreach ($answer->getOptions() as $option) {
                $options[] = $option->getId();
            }
            $newValue = $options;
        } else {
            $newMode = 'levenshtein';
            $newValue = $answer->getAnsweredValue();
        }
        return array($newMode, $newValue);
    }

}