<?php

namespace Trendix\RankBundle\Services;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Servicio para controlar todos los aspectos esenciales de una Rank
 * Class RankManager
 * @package Trendix\RankBundle\Services
 */
class RankManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Container
     */
    private $container;

    /**
     * RankManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * Genera los Form Types para cada tipo de criterion enviado por parámetro
     * @param $criterionTypes
     * @return array
     * @throws \Exception
     */
    public function createFormWithCriterionTypes($criterionTypes)
    {
        $formTypes = [];

        foreach ($criterionTypes as $key => $type) {
            $formTypes[$key] = $this->container->get('form.factory')->create($type, null, ['em' => $this->em])->createView();
        }

        return $formTypes;
    }
}