<?php

namespace Trendix\RankBundle\Services;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Trendix\RankBundle\Classes\Versions\VersionableInterface;
use Trendix\RankBundle\Entity\EvaluationForm;
use Trendix\RankBundle\Entity\Rank;
use Trendix\RankBundle\Entity\Versions\CriterionVersion;
use Trendix\RankBundle\Entity\Versions\SectionVersion;
use Trendix\RankBundle\Entity\Versions\Version;

class InheritVersionManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function inheritVersions(VersionableInterface $form)
    {
        $parent = $form->getParent();
        if (!$parent instanceof VersionableInterface) {
            return false;
        }

        $newVersions = new ArrayCollection();

        $versions = $parent->getVersions();
        if (!count($versions)) {
            return false;
        }

        /**
         * Recorremos las versiones del Formulario padre
         * @var Version $v
         */
        foreach ($versions as $v) {
            $newVersion = new Version($form);
            $newVersion->setSubcategoria($v->getSubcategoria());
            $newVersion->setAuthor($v->getAuthor());
            $newVersion->setPublishAt($v->getPublishAt());
            $this->inheritSections($newVersion, $v);
            $this->inheritCriteria($newVersion, $v);
            $newVersions->add($newVersion);
        }

        $form->setVersions($newVersions);

        $this->em->persist($form);
        $this->em->flush($form);
    }

    private function inheritSections(Version $new, Version $old)
    {
        /**
         * @var SectionVersion $s
         */
        foreach ($new->getSections() as $s) {
            /**
             * @var SectionVersion $os
             */
            $os = $old->findBySection($s->getSection());
            if (null !== $os) {
                $s->setVisible($os->isVisible());
                $s->setWeight($os->getWeight());
            }

        }
    }

    private function inheritCriteria(Version $new, Version $old)
    {
        /**
         * @var CriterionVersion $c
         */
        foreach ($new->getCriteria() as $c) {

            /**
             * @var CriterionVersion $oc
             */
            $oc = $old->findByCriterion($c->getCriterion());
            if (null !== $oc) {
                $c->setVisible($oc->isVisible());
            }
        }
    }
}