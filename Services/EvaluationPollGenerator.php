<?php

namespace Trendix\RankBundle\Services;


use Doctrine\ORM\EntityManager;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\Utility\Dates;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Entity\Answer\Answer;
use Trendix\RankBundle\Entity\Evaluation\EvaluationPoll;
use Trendix\RankBundle\Entity\EvaluationForm;
use Trendix\RankBundle\Entity\Rank;
use Trendix\RankBundle\Entity\SupplierClassification;
use Trendix\RankBundle\Entity\Versions\Version;
use Symfony\Component\DependencyInjection\Container;

class EvaluationPollGenerator
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var User
     */
    private $user;

    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function createPollFromEvaluation(EvaluationForm $evaluationForm, User $usuario)
    {
        $this->user = $usuario;
        // Si ya tiene una la devolvemos
        $evaluationPoll = $this->em->getRepository('TrendixRankBundle:Evaluation\EvaluationPoll')->findLastByUser($usuario);

        if (null !== $evaluationPoll && $evaluationForm->getId() == $evaluationPoll->getRank()->getId()) {
            $evaluationPoll = $this->updateClassification($evaluationPoll); //Actualizamos los criterios de clasificación si han cambiado
            $evaluationPoll = $this->updateVersion($evaluationPoll);
            return $evaluationPoll;
        }

        // Si no la tiene creamos una vacía
        $evaluationPoll = new EvaluationPoll();
        $evaluationPoll->setRank($evaluationForm)->setUser($usuario);
        $evaluationPoll = $this->updateVersion($evaluationPoll);

        // Añadimos las respuestas para los criterios de evaluacion
        $criteria = $this->em->getRepository('TrendixRankBundle:Rank')->findCriteriaFromRank($evaluationForm);
        foreach ($criteria as $criterion) {
            $evaluationPoll->addAnswer($this->container->get('evaluation.inherit.answers')->createAnswer($criterion, $this->user));
        }

        // Añadimos las respuestas para los criterios de clasificacion
        $classificationSection = $this->em->getRepository('TrendixRankBundle:SupplierClassification')->findCurrent();
        if (null !== $classificationSection) {
            $evaluationPoll->setSupplierClassification($classificationSection);
            $criteria = $this->em->getRepository('TrendixRankBundle:Rank')->findCriteriaFromRank($classificationSection);
            foreach ($criteria as $criterion) {
                $evaluationPoll->addAnswer($this->container->get('evaluation.inherit.answers')->createAnswer($criterion, $this->user));
            }
        }

        // Y la guardamos
        $this->em->persist($evaluationPoll);
        $this->em->flush($evaluationPoll);

        return $evaluationPoll;
    }

    private function createAnswer(CriterionTypeInterface $criterion)
    {
        $answerClass = $criterion::getAnswerClass();
        /**
         * @var Answer
         */
        $answer = new $answerClass();

        // Creación de la estructura de la respuesta
        $answer->setCriterion($criterion);

        return $answer;
    }

    /**
     * Actualiza los criterios de clasificación si la evaluación no está validada
     * @param EvaluationPoll $poll
     * @return EvaluationPoll
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function updateClassification(EvaluationPoll $poll)
    {
        // Si esta validada no hacemos nada
        $changed = false;
        if ($poll->getExpiresAt() instanceof \DateTime && $poll->getExpiresAt() > Dates::getDateWithMilisecs()) {
            return $poll;
        }


        // Añadimos las respuestas para los criterios de clasificacion
        $classificationSection = $this->em->getRepository('TrendixRankBundle:SupplierClassification')->findCurrent();
        if (null !== $classificationSection && $classificationSection != $poll->getSupplierClassification()) {
            $poll->setSupplierClassification($classificationSection);
            $poll = $this->cleanClassificationAnswers($poll);
            $criteria = $this->em->getRepository('TrendixRankBundle:Rank')->findCriteriaFromRank($classificationSection);
            foreach ($criteria as $criterion) {
                $poll->addAnswer($this->container->get('evaluation.inherit.answers')->createAnswer($criterion, $this->user));
                $changed = true;
            }

        }

        if ($changed) {
            $this->em->persist($poll);
            $this->em->flush($poll);
        }

        return $poll;
    }

    /**
     * Asigna o actualiza la versión del Formulario de evaluación según las categorías del usuario y el estado de la evaluación
     *
     * @param EvaluationPoll $poll
     * @return EvaluationPoll
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateVersion(EvaluationPoll $poll)
    {
        // Si esta validada no hacemos nada
        if ($poll->getExpiresAt() instanceof \DateTime && $poll->getExpiresAt() > Dates::getDateWithMilisecs() && $poll->getVersion() instanceof Version) {
            return $poll;
        }

        // Si ya tenía una version la eliminamos y creamos una nueva
        $oldVersion = $poll->getVersion();
        if (null !== $oldVersion) {
            $this->em->remove($oldVersion);
            $this->em->flush($oldVersion);
        }

        $versionGenerator = new CategoryVersion($poll);

        $poll->setVersion($versionGenerator->fusionVersion());

        return $poll;
    }

    /**
     * Elimina todas las respuestas que se hayan dado sobre la sección de clasificación
     *
     * @param EvaluationPoll $poll
     * @return EvaluationPoll
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function cleanClassificationAnswers(EvaluationPoll $poll)
    {
        $changed = false;
        $ids = [$poll->getRank()->getId(), $poll->getSupplierClassification()->getId()];
        foreach ($poll->getAnswers() as $answer) {
            /**
             * @var Answer $answer
             */
            if (!in_array($answer->getCriterion()->getSection()->getRank()->getId(), $ids)) {
                $poll->removeAnswer($answer);
                $this->em->remove($answer);
                $changed = true;
            }
        }

        if ($changed) {
            $this->em->persist($poll);
            $this->em->flush($poll);
        }

        //El entity Manager conserva rastros de la antigua entidad despues de modificada que puede alterar
        // El funcionamiento básico del formulario
        $this->em->refresh($poll);
        return $poll;
    }
}