<?php

namespace Trendix\RankBundle\Services;

use Doctrine\ORM\EntityManager;
use Trendix\SubastasBundle\Entity\Proveedor;
use Symfony\Component\DependencyInjection\Container;

class GraphicsManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine')->getManager();
    }

    /**
     * Obtiene la media de las valoraciones de un proveedor por meses
     */
    public function getAverageEvaluationFormData(\DateTime $startDate = null, \DateTime $endDate = null)
    {
        $ef = $this->em->getRepository('TrendixRankBundle:Evaluation\EvaluationPoll')
            ->findAllValidated($startDate, $endDate);

        $meses = $this->getMonthsArray($ef);
        $rankings = $this->getRankingFromMonthsArray($meses);
        $average = $this->getAverageArray($rankings);

        return $average;
    }

    public function getSupplierEvaluationFormData(Proveedor $proveedor, \DateTime $startDate = null, \DateTime $endDate = null)
    {
        $ef = $this->em->getRepository('TrendixRankBundle:Evaluation\EvaluationPoll')
            ->findAllValidatedBySupplier($proveedor, $startDate, $endDate);

        $rankings = $this->getRankingFromEvaluationsArray($ef);

        return $rankings;
    }

    /**
     * Obtiene la media de las valoraciones de un proveedor por meses
     */
    public function getAverageRecurrentFormData(Proveedor $proveedor, \DateTime $startDate = null, \DateTime $endDate = null)
    {
        $forms = $this->em->getRepository('TrendixRankBundle:RecurrentForm')->findByUser($proveedor->getUsuario());
        $recurrentAverage = [];

        foreach ($forms as $f) {
            $ef = $this->em->getRepository('TrendixRankBundle:Evaluation\RecurrentPoll')
                ->findAllValidated($f, $startDate, $endDate);

            $meses = $this->getMonthsArray($ef);
            $rankings = $this->getRankingFromMonthsArray($meses);
            $recurrentAverage[$f->getId()] = $this->getAverageArray($rankings);
        }

        return $recurrentAverage;
    }

    public function getSupplierRecurrentFormData(Proveedor $proveedor, \DateTime $startDate = null, \DateTime $endDate = null)
    {
        $forms = $this->em->getRepository('TrendixRankBundle:RecurrentForm')->findByUser($proveedor->getUsuario());
        $recurrentRanking = [];

        foreach ($forms as $f) {
            $ef = $this->em->getRepository('TrendixRankBundle:Evaluation\RecurrentPoll')
                ->findAllValidatedBySupplier($f, $proveedor, $startDate, $endDate);

            $recurrentRanking[$f->getId()] = $this->getRankingFromEvaluationsArray($ef);
        }

        return $recurrentRanking;
    }

    private function getMonthsArray($polls)
    {
        $meses = [];
        foreach ($polls as $p) {
            $year = $p->getUpdatedAt()->format('Y');
            $month = $p->getUpdatedAt()->format('m');
            $timeStamp = new \DateTime($year.'-'.$month.'-01');
            $timeStamp = $timeStamp->getTimestamp() * 1000;
            if (!isset($meses[$timeStamp])) {
                $meses[$timeStamp] = [];
            }
            $meses[$timeStamp][] = $p;
        }

        return $meses;
    }

    private function getRankingFromMonthsArray($meses)
    {
        $rankings = [];
        foreach ($meses as $mes => $m) {
            if (!isset($rankings[$mes])) {
                $rankings[$mes] = [];
            }
            foreach ($m as $ep) {
                $rank = $this->container->get('evaluation.ranking.calculator')->getArrayRankingFromPoll($ep);
                $rankings[$mes][(float) $ep->getUpdatedAt()->getTimeStamp() * 1000] = $rank['userPercent'];
            }
        }

        return $rankings;
    }

    private function getRankingFromEvaluationsArray($evaluations)
    {
        $rankings = [];
        foreach ($evaluations as $ep) {
            $rank = $this->container->get('evaluation.ranking.calculator')->getArrayRankingFromPoll($ep);
            $rankings[] = [$ep->getUpdatedAt()->getTimeStamp() * 1000, $rank['userPercent']];
        }

        return $rankings;
    }

    private function getAverageArray($rankings)
    {
        $average = [];
        foreach ($rankings as $mes => $r) {
            $average[] = [$mes, round(array_sum($r)/count($r), 2)];
        }

        return $average;
    }
}