<?php

namespace Trendix\RankBundle\Services;

use Doctrine\ORM\EntityManager;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\Utility\Dates;
use Trendix\RankBundle\Entity\Evaluation\EvaluationPoll;
use Trendix\RankBundle\Entity\Evaluation\Poll;
use Trendix\RankBundle\Entity\Evaluation\RecurrentPoll;
use Trendix\SubastasBundle\Entity\Proveedor;

class AccessManager
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Indica si un usuario puede diseñar un formulario de evaluación
     * @param User $user
     * @return bool
     */
    public function canEditEvaluationFromDesign(User $user)
    {
        return $user->isAdministrador();
    }

    /**
     * Indica si un usuario puede crear un formulario recurrente. Para ello debe ser Manager al menos de 1 Subcategoría
     * @param User $user
     * @return bool
     */
    public function canCreateRecurrentForm(User $user)
    {
        $managed = $this->em->getRepository('DirectorioBundle:Subcategoria')->findManagedByUser($user);
        return (count($managed) > 0);
    }

    /**
     * Indica si un usuario puede diseñar la sección de clasificación de proveedores
     * @param User $user
     * @return bool
     */
    public function canEditClassificationSectionDesign(User $user)
    {
        return $user->isAdministrador();
    }

    /**
     * Indica si un usuario puede rellenar un formulario de evaluación
     * @param User $user
     * @param EvaluationPoll $evaluationPoll
     * @return bool
     */
    public function canFillEvaluationPoll(User $user, EvaluationPoll $evaluationPoll)
    {
        if ($user->isComprador() || $user->isTecnico() && !$user->isAdministrador()) {
            return true;
        }

        if ($evaluationPoll->getUser() != $user) {
            return false;
        }

        $expirationDate = $evaluationPoll->getExpiresAt();

        if ($expirationDate instanceof \DateTime && $expirationDate > Dates::getDateTimeWithMilisecs()) {
            return false;
        }

        return true;
    }

    public function canFillRecurrentPoll(User $user, RecurrentPoll $poll)
    {
        if ($user->isComprador() || $user->isTecnico() && !$user->isAdministrador()) {
            return true;
        }

        if ($poll->getUser() != $user) {
            return false;
        }

        return true;
    }

    /**
     * Indica si un usuario puede ver un formulario de evaluación
     * @param User $user
     * @param EvaluationPoll $evaluationPoll
     * @return bool
     */
    public function canViewEvaluationPoll(User $user, EvaluationPoll $evaluationPoll)
    {
        if ($user->isComprador()) {
            return true;
        }

        if ($evaluationPoll->getUser() != $user) {
            return false;
        }

        return true;
    }

    /**
     * Indica si el usuario puede acceder a la lista de formularios recurrentes
     * @param User $user
     * @return bool
     */
    public function canViewRecurrentFormList(User $user)
    {
        return $user->isComprador() || $user->isAdministrador() || $user->isAutorizador();
    }

    /**
     * @param User $user
     * @param Poll $poll
     * @return bool
     */
    public function canValidateRecurrentPoll(User $user, Poll $poll)
    {
        $owner = $poll->getUser();
        if ($owner->getProveedor() instanceof Proveedor) {
            $proveedor = $owner->getProveedor();
            $managers = $this->em->getRepository('DirectorioBundle:ManagerProveedor')->findBy(['proveedor' => $proveedor]);

            foreach ($managers as $m) {
                if ($m->getUsuario()->getId() === $user->getId()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param User $user
     * @param Poll $poll
     * @return bool
     */
    public function canValidateEvaluationPoll(User $user, Poll $poll)
    {
        // Por el momento son identicas
        return $this->canValidateRecurrentPoll($user, $poll);
    }
}