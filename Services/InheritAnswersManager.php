<?php

namespace Trendix\RankBundle\Services;

use Doctrine\ORM\EntityManager;
use Trendix\AdminBundle\Entity\User;
use Trendix\RankBundle\Classes\Answer\AnswerAbstractType;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Entity\Answer\Answer;
use Trendix\RankBundle\InheritAnswer\VoidInheritAnswer;

class InheritAnswersManager
{
    const INHERIT_NAMESPACE = 'Trendix\\RankBundle\\InheritAnswer\\';

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Crea la respuesta a partir de la respuesta de su criterio padre. Si no tiene crea una respuesta vacía
     * @param CriterionTypeInterface $criterion
     * @param User $user
     * @return \Trendix\RankBundle\Classes\Answer\AnswerTypeInterface|mixed
     */
    public function createAnswer(CriterionTypeInterface $criterion, $user)
    {
        $parent = $criterion->getParent();
        if (null === $parent) {
            return $this->createCleanAnswer($criterion);
        }

        $answer = $this->em->getRepository('TrendixRankBundle:Answer\Answer')->findByCrieterionAnsUser($parent, $user);
        if (null === $answer) {
            return $this->createCleanAnswer($criterion);
        }

        $newAnswer = $this->inheritAnswerType($answer, $criterion);

        return $newAnswer;
    }

    /**
     * Devuelve una respuesta vacía del tipo del criterio pasado por parámetro
     * @param CriterionTypeInterface $criterion
     * @return mixed
     */
    public function createCleanAnswer(CriterionTypeInterface $criterion)
    {
        $answerClass = $criterion::getAnswerClass();

        /**
         * @var Answer
         */
        $answer = new $answerClass();

        // Creación de la estructura de la respuesta
        $answer->setCriterion($criterion);

        return $answer;
    }

    /**
     * Clona la respuesta "padre" para asociarla al criterio "hijo"
     * @param AnswerAbstractType $parent
     * @param CriterionTypeInterface $criterion
     * @return \Trendix\RankBundle\Classes\Answer\AnswerTypeInterface
     */
    private function inheritAnswerType(AnswerAbstractType $parent, CriterionTypeInterface $criterion)
    {
        $child = clone $parent;
        $child->setCriterion($criterion);

        $inherit = $this->getInheritFromAnswer($child);

        return $inherit->transferAnswer($parent, $child);
    }

    /**
     * Devuelve el protocolo de herencia (inheritAnswer) segun el tipo de respuesta.
     * Devuelve el protocolo Void o estándar si el tipo de respuesta no tiene protocolo definido
     * @param $answer
     * @return VoidInheritAnswer
     */
    private function getInheritFromAnswer($answer)
    {
        $className = $this->getSingleClassName($answer);
        $inheritClassName = str_replace('Answer', 'InheritAnswer', $className);
        if (class_exists(self::INHERIT_NAMESPACE.$inheritClassName)) {
            $inheritFullClassName = self::INHERIT_NAMESPACE.$inheritClassName;
            return new $inheritFullClassName;
        }

        return new VoidInheritAnswer();
    }

    /**
     * Devuelve el nombre de una clase sin namespace
     * @param $object
     * @return mixed
     */
    private function getSingleClassName($object)
    {
        $path = explode('\\', get_class($object));
        return array_pop($path);
    }
}