<?php

/**
 * El método de calculo de ranking utiliza el "Algoritmo de la mochila"
 * Este se encarga de ir haciendo calculo parciales y almacenarlos para finalmente obtener el resultado deseado
 * Aprovechando este método vamos obteniendo también las puntuaciones de cada respuesta realizada y las de cada
 * sección sin aumentar el coste de tiempo del cálculo
 *
 * @see https://es.wikibooks.org/wiki/Programaci%C3%B3n_din%C3%A1mica/Problema_de_la_mochila
 */

namespace Trendix\RankBundle\Services;

use Doctrine\ORM\EntityManager;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\Utility\Dates;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Classes\Ranking\RankingTypedInterface;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Evaluation\Poll;
use Trendix\RankBundle\Entity\Rank;
use Symfony\Component\Translation\TranslatorInterface;

class Ranking
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TranslatorInterface
     */
    private $trans;

    /**
     * @var Poll
     */
    private $poll;

    /**
     * @var Rank
     */
    private $rank;

    /**
     * Indica si se han respondido a todas las preguntas
     * @var boolean
     */
    private $completed;

    public function __construct(EntityManager $em, TranslatorInterface $trans)
    {
        $this->em = $em;
        $this->trans = $trans;
        $this->completed = true;
    }

    /**
     * Devuelve un array asociativo de preguntas con las puntuaciones obtenidas
     * por el proveedor para cada una de ellas y el máximo total
     * agrupado por secciones
     *
     * @param User $user
     *
     * @return array
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getArrayRanking(User $user)
    {
        $ranking = $this->getArrayRankingSkeleton();
        $this->poll = $this->getPollFromUser($user);
        if (null === $this->poll) {
            return $ranking;
        }

        return $this->getArrayRankingFromPoll($this->poll);
    }

    public function getArrayRankingFromPoll(Poll $poll)
    {
        $this->completed = true;
        $ranking = $this->getArrayRankingSkeleton();
        $this->poll = $poll;
        if (null === $this->poll) {
            return $ranking;
        }

        $totalCriteria = 0;
        $totalAnswered = 0;

        $this->rank = $this->poll->getRank();
        $sections = $this->em->getRepository(get_class($poll)) //Usamos getClass ya que cada poll debe tener su propio findAllCurrentSections
            ->findAllCurrentSections($poll);
        foreach ($sections as $section) {
            $ranking['sections'][$section->getId()] = $this->getArrayRankingSection($section);
            $ranking['maxPoints'] += $ranking['sections'][$section->getId()]['maxPoints'];
            $ranking['userPoints'] += $ranking['sections'][$section->getId()]['userPoints'];
            $ranking['userPercent'] += $ranking['sections'][$section->getId()]['userPercent'];

            $totalCriteria += $section->getCriteria()->count();
            $totalAnswered += $ranking['sections'][$section->getId()]['totalAnswered'];
        }

        $ranking['completed'] = $this->completed;
        $ranking['completedPercent'] = ($totalAnswered / $totalCriteria) * 100;
        $ranking['poll'] = $this->poll;

        if ($this->completed) {
            $ranking['status'] = 'info';
            $ranking['statusText'] = 'evaluations.completed';
        }

        if ($this->isValidatedPoll()) {
            $ranking['status'] = 'success';
            $ranking['statusText'] = $this->trans->trans('str_valido_hasta').': '.$this->poll->expirationDate()->format('d-m-Y');
        }

        return $ranking;
    }

    /**
     * Devuelve true si el formulario tiene fecha de expiración y es mayor que la fecha actual
     * @return bool
     */
    private function isValidatedPoll()
    {
        return $this->poll->isValid();
    }

    private function getArrayRankingSection($section)
    {
        $ranking = $this->getArraySectionSkeleton();
        $ranking['maxPercent'] = $section->getWeight();

        $maxSectionPoints = 0;
        $userSectionPoints = 0;
        $totalAnswered = 0;

        foreach ($section->getCriteria() as $criterion) {
            $ranking['criteria'][$criterion->getId()] = $this->getArrayRankingCriterion($criterion);
            $maxSectionPoints += $ranking['criteria'][$criterion->getId()]['maxPoints'];
            $userSectionPoints += $ranking['criteria'][$criterion->getId()]['userPoints'];
            $totalAnswered += ($ranking['criteria'][$criterion->getId()]['answered'] ? 1 : 0);
        }
        $ranking['maxPoints'] = $maxSectionPoints;
        $ranking['userPoints'] = $userSectionPoints;
        $ranking['totalAnswered'] = $totalAnswered;

        if ($ranking['maxPoints'] == 0) {
            $ranking['userPercent'] = 0;
        } else {
            $ranking['userPercent'] = ($userSectionPoints/$maxSectionPoints * $section->getWeight());
        }

        return $ranking;
    }

    /**
     * @param Criterion $criterion
     *
     * @return array
     *
     * @throws \Exception
     */
    private function getArrayRankingCriterion(Criterion $criterion)
    {
        $ranking = $this->getArrayCriterionSkeleton();
        $answer = $this->findAnswerFromCriterion($criterion);
        if (!$answer instanceof AnswerTypeInterface) {
            // En el momento que no encontremos una respuesta a una pregunta marcamos la evaluación como incompleta
            $this->completed = false;
            return $ranking;
        }

        $ranking['answered'] = $answer->isFilled();
        if (!$answer->isFilled()) {
            $this->completed = false;
        }
        $formulaClassName = $criterion->getFormulaClass();
        $formula = new $formulaClassName($criterion, $answer);

        if (!$formula instanceof RankingTypedInterface) {
            throw new \Exception('La formula debe ser una instancia de "RankingTypedInterface"');
        }

        $ranking['maxPoints'] = $formula->getMaxPoints();
        if ($ranking['answered']) {
            $ranking['userPoints'] = $formula->getPoints();
        }

        return $ranking;
    }

    /**
     * Devuelve el porcentaje de puntos que tiene un usuario en función de su última evaluación
     * Si no tiene evaluación o no la ha completado devuelve false
     * @param User $user
     *
     * @return float|boolean
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getRanking(User $user)
    {
        $ranking = $this->getArrayRanking($user);
        if (!$ranking['completed']) {
            return false;
        }
        return $ranking['userPercent'];
    }

    public function getLevel(User $user)
    {
        $punctuation = $this->getRanking($user);
        return $this->em->getRepository('TrendixRankBundle:Levels\Level')->findLevelFromPunctuation((float) $punctuation);
    }

    /**
     * Devuleve la estructura base que tendrá el array de ranking
     * @return array
     */
    private function getArrayRankingSkeleton()
    {
        return [
            'poll' => null,
            'completed' => false,
            'status' => 'warning',
            'statusText' => 'evaluations.pending',
            'sections' => [],
            'maxPoints' => 0,
            'userPoints' => 0,
            'userPercent' => 0, // Puntuación final en % (con la ponderación de las secciones incluida)
            'completedPercent' => 0 //Indica el porcentaje del formulario que esta completado
        ];
    }

    private function getArraySectionSkeleton()
    {
        return [
            'criteria' => [],
            'maxPercent' => 0,
            'userPercent' => 0,
            'maxPoints' => 0,
            'userPoints' => 0,
            'totalAnswered' => 0
        ];
    }

    private function getArrayCriterionSkeleton()
    {
        return [
            'answered' => false,
            'maxPoints' => 0,
            'userPoints' => 0,
        ];
    }

    /**
     * Busca la respuesta a un criterio
     * @param Criterion $criterion
     * @return AnswerTypeInterface|null
     */
    private function findAnswerFromCriterion(Criterion $criterion)
    {
        foreach ($this->poll->getAnswers() as $answer) {
            if ($answer->getCriterion()->getId() == $criterion->getid()) {
                return $answer;
            }
        }

        return null;
    }

    /**
     * Obtiene la ultima evaluación completada por un usuario
     * @param User $user
     * @return Poll
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getPollFromUser(User $user)
    {
        return $this->em->getRepository('TrendixRankBundle:Evaluation\EvaluationPoll')
            ->findLastByUser($user);
    }
}