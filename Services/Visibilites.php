<?php

namespace Trendix\RankBundle\Services;

use Doctrine\ORM\EntityManager;
use Trendix\AdminBundle\Entity\User;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Classes\Rank\CriterionFillerInterface;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Evaluation\EvaluationPoll;
use Trendix\RankBundle\Entity\Evaluation\Poll;
use Trendix\RankBundle\Entity\Rank;
use Trendix\RankBundle\Entity\Section;
use Trendix\RankBundle\Entity\SupplierClassification;
use Trendix\RankBundle\Entity\Versions\Version;

class Visibilites
{
    /**
     * @var EntityManager
     */
    private $em;

    private $poll;

    private $version;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->version = null;
    }

    public function setPoll(Poll $poll)
    {
        $this->poll = $poll;
        if ($this->poll instanceof EvaluationPoll) {
            $this->version = $this->poll->getVersion();
        }
    }

    /**
     * Devuelve un array asociativo con las visibilidades de criterios y secciones para un usuario concreto
     *
     * @param User $user
     * @param Poll $poll
     * @return array
     */
    public function getVisibilities(User $user, Poll $poll)
    {
        $this->poll = $poll;
        $visibilities = [];
        if ($poll instanceof EvaluationPoll) {
            $sections = $this->em->getRepository('TrendixRankBundle:Evaluation\EvaluationPoll')
                ->findAllCurrentSections($poll);
        } else {
            $sections = $this->em->getRepository('TrendixRankBundle:Evaluation\Poll')
                ->findAllCurrentSections($poll);
        }

        foreach ($sections as $section) {
            $visibilities[$section->getId()] = [
                'visible' => $this->isSectionVisibleByUser($user, $section),
                'criteria' => []
            ];

            foreach ($section->getCriteria() as $criterion) {
                $visibilities[$section->getId()]['criteria'][$criterion->getId()] = $this->isCriterionVisibleByUser($user, $criterion);
            }
        }

        return $visibilities;
    }

    /**
     * Dvuelve true o false en función de si un usuario puede ver un criterio o no
     * @param User $user
     * @param Section $section
     * @return bool
     */
    public function isSectionVisibleByUser(User $user, Section $section)
    {
        // Comprobamos si la sección está seleccionada en las versiones
        if (!$this->isSectionInCategories($section) && !$section->getRank() instanceof SupplierClassification) {
            return false;
        }

        // Si esta en las versiones y el usuario es comprador la puede ver
        if ($user->isComprador()) {
            return true;
        }

        // Si no está en las versiones y el usuario es proveedor se comprueba el filler
        return $section->isVisibleProveedor();
    }

    /**
     * Devuelve true o false en función de si un usuario puede ver un criterio o no
     *
     * @param User $user
     * @param Criterion $criterion
     * @return bool
     */
    public function isCriterionVisibleByUser(User $user, Criterion $criterion)
    {
        if (!$this->isCriterionVisibleInCategories($criterion)) {
            return false;
        }

        if ($user->isComprador() || $user->isTecnico()) {
            return true;
        }

        return ($criterion->getFiller() == CriterionFillerInterface::FILLER_SUPLIER);
    }

    /**
     * Indica si el usuario puede ver la fecha de expiración de un formulario de proveedor
     * @param User $user
     * @return bool
     */
    public function isExpiredDateVisibleByUser(User $user)
    {
        return $user->isComprador();
    }

    private function isCriterionVisibleInCategories(CriterionTypeInterface $criterion)
    {
        if (!$this->version instanceof Version) {
            return true;
        }

        return $this->version->isCriterionVisible($criterion);
    }

    /**
     * Devuelve si la sección es visible en las categorias seleccionadas
     * @param Section $section
     * @return bool
     */
    private function isSectionInCategories(Section $section)
    {
        if (!$this->version instanceof Version) {
            return true;
        }

        return $this->version->isSectionVisible($section);
    }
}
