<?php

namespace Trendix\RankBundle\Entity\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Trendix\RankBundle\Entity\Rank;

/**
 * RankRepository
 *
 * Funciones útiles y necesarias para obtener valores importantes de una Rank
 * Cuando hablamos de criterios y bloques activos nos referimos a los asociados a la última versión de la misma
 */
class RankRepository extends EntityRepository
{
    public function findCurrent()
    {
        $dql = "
        SELECT r FROM TrendixRankBundle:Rank r 
        WHERE r.draft = false
        ORDER BY r.id DESC
        ";

        return $this->_em->createQuery($dql)->setMaxResults(1)->getOneOrNullResult();
    }

    public function findLastToDesign()
    {
        $dql = "
        SELECT r FROM TrendixRankBundle:Rank r 
        ORDER BY r.id DESC
        ";

        $rank = $this->_em->createQuery($dql)->setMaxResults(1)->getOneOrNullResult();

        if (null === $rank) {
            return new Rank();
        }

        if(!$rank->isDraft()) {
            //return clone $rank;
        }

        return $rank;
    }

    /**
     * Devuelve un ArrayCollection con los tipos de criterios de una Rank
     * TODO Hacer esto como un DQL (ver si es posible)
     * @param Rank $rank
     * @return ArrayCollection
     */
    public function findCriteriaFromRank(Rank $rank)
    {
        $criteria = new ArrayCollection();

        foreach ($rank->getSections() as $section) {
            foreach ($section->getCriteria() as $criterion) {
                $criteria->add($criterion);
            }
        }

        return $criteria;
    }
}
