<?php

namespace Trendix\RankBundle\Entity\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Trendix\RankBundle\Entity\Answer\FileAnswer;
use Trendix\RankBundle\Entity\Criterion\FileCriterion;
use Trendix\RankBundle\Entity\Poll\Poll;
use Trendix\RankBundle\Entity\EvaluationForm;
use Trendix\RankBundle\Entity\Rank;
use Trendix\SubastasBundle\Entity\Proveedor;

class PollRepository extends EntityRepository
{
    /**
     * Devuelve un arraycollection con todas las secciones que se necesitan para responder el formulario
     * Ordenadas por orden y tipo
     * @param Poll $poll
     * @return ArrayCollection
     */
    public function findAllCurrentSections(Poll $poll)
    {
        $sections = new ArrayCollection();
        $Rank = $poll->getRank();

        if (null !== $Rank) {
            $sections = clone $Rank->getSections();
        }

        return $sections;
    }

    public function getDocumentacionFormularios(Proveedor $proveedor)
    {
        $ficherosArray = [];
        $formRepo = $this->_em->getRepository('TrendixRankBundle:RecurrentForm');
        $eFormRepo = $this->_em->getRepository('TrendixRankBundle:EvaluationForm');

        $form = $eFormRepo->findCurrent();
        if ($form instanceof EvaluationForm) {
            $ficherosArray[] = $this->getArrayDocumentacionForm($form, $proveedor, Poll::TYPE_EVALUATIONPOLL);
        }

        $forms = $formRepo->findPendingByUser($proveedor->getUsuario());
        foreach ($forms as $form) {
            $ficherosArray[] = $this->getArrayDocumentacionForm($form, $proveedor);
        }
        return $ficherosArray;
    }

    public function getArrayDocumentacionForm(Rank $form, Proveedor $proveedor, $type = Poll::TYPE_RECURRENTPOLL)
    {
        $ePollRepo = $this->_em->getRepository('TrendixRankBundle:Evaluation\EvaluationPoll');
        $rPollRepo = $this->_em->getRepository('TrendixRankBundle:Evaluation\RecurrentPoll');

        $array = $this->getArrayDocumentacionSkeleton();
        if ($type == Poll::TYPE_EVALUATIONPOLL) {
            $poll = $ePollRepo->findLastByUser($proveedor->getUsuario());
            $array['title'] = 'evaluations.evaluation_form';
        } elseif ($type == Poll::TYPE_RECURRENTPOLL) {
            $poll = $rPollRepo->findLastByUser($proveedor->getUsuario(), $form);
            $array['title'] = $form->getTitle();
        }

        foreach ($form->getSections() as $section) {
            foreach ($section->getCriteria() as $criterion) {
                if ($criterion instanceof FileCriterion) {
                    $document = $this->getArrayDocumentSkeleton();
                    $document['title'] = $criterion->getTitle();
                    $document['files'] = $this->getArrayDocumentCriterion($criterion, $poll);
                    $document['expiration_date'] = ($poll instanceof Poll) ? $poll->expirationDate() : null;
                    $array['documents'][] = $document;
                }
            }
        }

        return $array;
    }

    public function getArrayDocumentCriterion($criterion, $poll)
    {
        $documents = [];
        if (null === $poll) {
            return $documents;
        }

        foreach ($poll->getAnswers() as $answer) {
            if ($answer->getCriterion() == $criterion) {
                $documents = $answer->getFiles()->toArray();
            }
        }

        return $documents;
    }

    public function getArrayDocumentacionSkeleton()
    {
        return [
            'title' => '',
            'documents' => [],
            'expiration_date' => null
        ];
    }

    public function getArrayDocumentSkeleton()
    {
        return [
          'title' => '',
          'files' => []
        ];
    }
}