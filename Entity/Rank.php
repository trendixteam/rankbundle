<?php

namespace Trendix\RankBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\EventListener\DiscriminatorEntry;
/*
 * @ORM\DiscriminatorMap{
 *     "EVALUATIONFORM" = "Trendix\RankBundle\Entity\EvaluationForm",
 *     "SUPPLIERCLASSIFICATION" = "Trendix\RankBundle\Entity\SupplierClassification",
 *     "RECURRENTFORM" = "Trendix\RankBundle\Entity\RecurrentForm"
 * }
 */
/**
 * Rank
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Trendix\RankBundle\Entity\Repository\RankRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name = "type", type = "string")
 * @DiscriminatorEntry(value = "rank")
 */
class Rank
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Rank
     * @ORM\OneToOne(targetEntity="Trendix\RankBundle\Entity\Rank")
     */
    protected $parent;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Trendix\RankBundle\Entity\Poll\Poll", mappedBy="rank")
     */
    protected $polls;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    protected $updated_at;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Trendix\RankBundle\Entity\Section", mappedBy="rank", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $sections;

    /**
     * @var boolean
     *
     * @ORM\Column(name="draft", type="boolean")
     */
    protected $draft;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->sections = new ArrayCollection();
        $this->sections->add(new Section());
        $this->polls = new ArrayCollection();
        $this->draft = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Rank
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Rank $parent
     * @return Rank
     */
    public function setParent(Rank $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPolls()
    {
        return $this->polls;
    }

    /**
     * @param ArrayCollection $polls
     * @return Rank
     */
    public function setPolls(ArrayCollection $polls): Rank
    {
        $this->polls = $polls;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     * @return Rank
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \DateTime $updated_at
     * @return Rank
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * @param ArrayCollection $sections
     * @return Rank
     */
    public function setSections($sections)
    {
        $this->sections = $sections;
        return $this;
    }

    /**
     * @param Section $section
     *
     * @return $this
     */
    public function addSection(Section $section)
    {
        $section->setRank($this);
        $this->sections->add($section);

        return $this;
    }

    /**
     * @param Section $section
     *
     * @return $this
     */
    public function removeSection(Section $section)
    {
        if ($this->sections->contains($section)) {
            $this->sections->removeElement($section);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isDraft()
    {
        return $this->draft;
    }

    /**
     * @param bool $draft
     * @return Rank
     */
    public function setDraft($draft)
    {
        $this->draft = $draft;
        return $this;
    }

    public function __toString()
    {
        return (string) $this->id;
    }

    public function cloneWithParent(Rank $Rank)
    {
        $Rank->setParent($Rank);
        return clone $Rank;
    }

    public function __clone() {
        if ($this->id) {
            // Eliminamos el id para que se genere una nueva fila en base de datos
            $this->id = null;
            $this->created_at = new \DateTime();

            // clonamos las secciones hijas
            $newSections = new ArrayCollection();
            foreach ($this->sections as $section) {
                $newSection = clone $section;
                $newSection->setRank($this);
                $newSection->setParent($section);
                $newSections->add($newSection);
            }
            $this->sections = $newSections;
        }
    }
}
