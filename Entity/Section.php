<?php

namespace Trendix\RankBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Trendix\AdminBundle\Utility\Numeric;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Classes\Rank\CriterionFillerInterface;

/**
 * Section
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Trendix\RankBundle\Entity\Repository\SectionRepository")
 */
class Section implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Section
     * @ORM\OneToOne(targetEntity="Trendix\RankBundle\Entity\Section")
     */
    private $parent;

    /**
     * @var Rank
     *
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Rank", inversedBy="sections" )
     */
    private $rank;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float")
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Trendix\RankBundle\Entity\Criterion\Criterion", cascade={"persist", "remove"}, mappedBy="section", orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $criteria;

    public function __construct() {
    	$this->criteria = new ArrayCollection();
    }

    /**
     * @return Rank
     */
    public function getRank(): Rank
    {
        return $this->rank;
    }

    /**
     * @param Rank $rank
     * @return Section
     */
    public function setRank(Rank $rank): Section
    {
        $this->rank = $rank;
        return $this;
    }

	/**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Section
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Section $parent
     * @return Section
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Section
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Section
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return Section
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return Section
     */
    public function setPosition(int $position): Section
    {
        $this->position = $position;
        return $this;
    }

    public function getNumerationAlias()
    {
        return Numeric::getLetterFromNumber($this->getPosition()-1);
    }

    /**
     * @return ArrayCollection
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param ArrayCollection $criteria
     * @return Section
     */
    public function setCriteria($criteria)
    {
        $this->criteria = $criteria;
        return $this;
    }

	/**
	 * @param Criterion $criterion
	 *
	 * @return $this
	 */
    public function addCriterion(CriterionTypeInterface $criterion)
    {
    	$criterion->setSection($this);
    	$this->criteria->add($criterion);

    	return $this;
    }

	/**
	 * @param CriterionTypeInterface $criterion
	 *
	 * @return $this
	 */
    public function removeCriterion(CriterionTypeInterface $criterion)
    {
        if ($this->criteria->contains($criterion)) {
            $this->criteria->removeElement($criterion);
        }

        return $this;
    }

    /**
     * Dice si una sección es visible por un proveedor
     * @return bool
     */
    public function isVisibleProveedor()
    {
        foreach ($this->criteria as $criterion) {
            if ($criterion->getFiller() == CriterionFillerInterface::FILLER_SUPLIER) {
                return true;
            }
        }

        return false;
    }

    public function __clone() {
        if ($this->id) {
            // Eliminamos el id para que se genere una nueva fila en base de datos
            $this->id = null;

            // Clonamos los criterios hijos
            $newCriteria = new ArrayCollection();
            foreach ($this->criteria as $criterion) {
                $newCriterion = clone $criterion;
                $newCriterion->setSection($this);
                $newCriterion->setParent($criterion);
                $newCriteria->add($newCriterion);
            }
            $this->criteria = $newCriteria;
        }
    }

    public function __toString()
    {
        return $this->title;
    }

    public function jsonSerialize()
    {
        $criteria = [];
        foreach ($this->criteria as $criterion) {
            $criteria[] = $criterion->jsonSerialize();
        }

        return [
            'id' => $this->id,
            'position' => $this->position,
            'title' => $this->title,
            'description' => $this->description,
            'criteria' => $criteria
        ];
    }
}
