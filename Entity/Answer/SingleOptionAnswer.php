<?php

namespace Trendix\RankBundle\Entity\Answer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Classes\Answer\AnswerAbstractType;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;
use Trendix\RankBundle\Entity\Criterion\Option;


/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class SingleOptionAnswer extends Answer implements AnswerTypeInterface
{
    /**
     * @var Option
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Criterion\Option")
     */
    private $option;

    /**
     * @return Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param Option $option
     * @return SingleOptionAnswer
     */
    public function setOption($option)
    {
        $this->option = $option;
        return $this;
    }

    public function isFilled()
    {
        return (null !== $this->option);
    }

    public static function getType()
    {
        return self::TYPE_SINGLE_OPTION;
    }

    public static function getFormTypeClass()
    {
        return self::getFormTypes()[self::getType()];
    }

    /**
     * @inheritdoc
     */
    public function getAnsweredValue()
    {
        return $this->option->getTitle();
    }

    public function setAnsweredValue($option)
    {
        $this->setOption($option);
        return parent::setAnsweredValue($option);
    }
}