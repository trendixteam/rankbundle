<?php

namespace Trendix\RankBundle\Entity\Answer;

use Doctrine\Common\Collections\ArrayCollection;
use Trendix\AdminBundle\Entity\Fichero;
use Trendix\RankBundle\Classes\Answer\AnswerAbstractType;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class FileAnswer extends Answer implements AnswerTypeInterface
{
    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Trendix\AdminBundle\Entity\TrendixFile", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinTable(name="fileanswer_files",
     *      joinColumns={@ORM\JoinColumn(name="fileanswer_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_fichero", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    private $files;

    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param ArrayCollection $files
     * @return FileAnswer
     */
    public function setFiles(ArrayCollection $files)
    {
        $this->files = $files;
        return $this;
    }

    /**
     * @param Fichero $file
     * @return $this
     */
    public function addFile($file)
    {
        $this->files->add($file);
        return $this;
    }

    /**
     * @param Fichero $file
     * @return $this
     */
    public function removeFile($file)
    {
        if ($this->files->contains($file)) {
            $this->files->removeElement($file);
        }
        return $this;
    }

    public function isFilled()
    {
        return ($this->files->count() > 0);
    }

    public static function getType()
    {
        return self::TYPE_FILE;
    }

    public static function getFormTypeClass()
    {
        return self::getFormTypes()[self::getType()];
    }

    /**
     * @inheritdoc
     */
    public function getAnsweredValue()
    {
        return $this->files;
    }

    public function setAnsweredValue($value)
    {
        $this->setFiles($value);
        return parent::setAnsweredValue($value);
    }
}
