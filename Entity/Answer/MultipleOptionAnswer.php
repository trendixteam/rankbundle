<?php

namespace Trendix\RankBundle\Entity\Answer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Classes\Answer\AnswerAbstractType;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;


/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class MultipleOptionAnswer extends Answer implements AnswerTypeInterface
{
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Trendix\RankBundle\Entity\Criterion\Option")
     */
    private $options;

    /**
     * @return ArrayCollection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param ArrayCollection $options
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function isFilled()
    {
        if (!$this->options instanceof Collection) {
            return false;
        }

        return ($this->options->count() > 0);
    }


    public static function getType()
    {
        return self::TYPE_MULTIPLE_OPTION;
    }

    public static function getFormTypeClass()
    {
        return self::getFormTypes()[self::getType()];
    }

    /**
     * @inheritdoc
     */
    public function getAnsweredValue()
    {
        return $this->options;
    }

    public function setAnsweredValue($options)
    {
        $this->setOptions($options);
        return parent::setAnsweredValue($options);
    }
}