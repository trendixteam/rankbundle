<?php


namespace Trendix\RankBundle\Entity\Answer;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Classes\Answer\AnswerAbstractType;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class RangeAnswer extends Answer implements AnswerTypeInterface
{
    /**
     * @var float
     * @ORM\Column(name="value", type="float", nullable=true)
     */
    private $value;

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return RangeAnswer
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public static function getType()
    {
        return self::TYPE_RANGE;
    }

    public function isFilled()
    {
        return (null !== $this->value);
    }

    public static function getFormTypeClass()
    {
        return self::getFormTypes()[self::getType()];
    }

    /**
     * @inheritdoc
     */
    public function getAnsweredValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return (string) $this->value;
    }

    public function setAnsweredValue($value)
    {
        $this->setValue($value);
        return parent::setAnsweredValue($value);
    }
}