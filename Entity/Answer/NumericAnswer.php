<?php

namespace Trendix\RankBundle\Entity\Answer;

use Trendix\RankBundle\Classes\Answer\AnswerAbstractType;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Classes\Answer\AnswerTypeInterface;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class NumericAnswer extends Answer implements AnswerTypeInterface
{
    /**
     * @var float
     * @ORM\Column(name="value", type="float", nullable=true)
     */
    private $value;

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function isFilled()
    {
        return (null !== $this->value);
    }

    public static function getType()
    {
        return self::TYPE_NUMERIC;
    }

    public static function getFormTypeClass()
    {
        return self::getFormTypes()[self::getType()];
    }

    /**
     * @inheritdoc
     */
    public function getAnsweredValue()
    {
        return $this->value;
    }

    public function setAnsweredValue($value)
    {
        $this->setValue($value);
        return parent::setAnsweredValue($value);
    }
}