<?php

namespace Trendix\RankBundle\Entity\Answer;

use Doctrine\ORM\Mapping as ORM;
use Trendix\AdminBundle\Entity\User;
use Trendix\RankBundle\Entity\Criterion\Criterion;
use Trendix\RankBundle\Entity\Poll\Poll;
use Trendix\RankBundle\Form\Answer\TextAnswerType;
use Trendix\RankBundle\Form\Answer\FileType;
use Trendix\RankBundle\Form\Answer\MultipleOptionType;
use Trendix\RankBundle\Form\Answer\NumericType;
use Trendix\RankBundle\Form\Answer\RangeType;
use Trendix\RankBundle\Form\Answer\SingleOptionType;
use Trendix\RankBundle\Form\Answer\TextType;

/**
 * Answer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Trendix\RankBundle\Entity\Repository\AnswerRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name = "type", type = "string")
 * @ORM\DiscriminatorMap({
 *     "Answer::TYPE_NUMERIC" = "Trendix\RankBundle\Entity\Answer\NumericAnswer",
 *     "Answer::TYPE_RANGE" = "Trendix\RankBundle\Entity\Answer\RangeAnswer",
 *     "Answer::TYPE_SINGLE_OPTION" = "Trendix\RankBundle\Entity\Answer\SingleOptionAnswer",
 *     "Answer::TYPE_MULTIPLE_OPTION" = "Trendix\RankBundle\Entity\Answer\MultipleOptionAnswer",
 *     "Answer::TYPE_TEXT" = "Trendix\RankBundle\Entity\Answer\TextAnswer",
 *     "Answer::TYPE_FILE" = "Trendix\RankBundle\Entity\Answer\FileAnswer"
 * })
 */
abstract class Answer
{
    const TYPE_NUMERIC = Criterion::TYPE_NUMERIC;
    const TYPE_RANGE = Criterion::TYPE_RANGE;
    const TYPE_SINGLE_OPTION = Criterion::TYPE_SINGLE_OPTION;
    const TYPE_MULTIPLE_OPTION = Criterion::TYPE_MULTIPLE_OPTION;
    const TYPE_TEXT = Criterion::TYPE_TEXT;
    const TYPE_FILE = Criterion::TYPE_FILE;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Criterion
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Criterion\Criterion", inversedBy="answers")
     */
    private $criterion;

    /**
     * @var Poll
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Poll\Poll", inversedBy="answers")
     */
    private $poll;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Criterion
     */
    public function getCriterion()
    {
        return $this->criterion;
    }

    /**
     * @param Criterion $criterion
     * @return Answer
     */
    public function setCriterion($criterion)
    {
        if($criterion instanceof Criterion) {
            $criterion->addAnswer($this);
            $this->criterion = $criterion;
        }
        return $this;
    }

    /**
     * @return Poll
     */
    public function getPoll(): Poll
    {
        return $this->poll;
    }

    /**
     * @param Poll $poll
     * @return Answer
     */
    public function setPoll(Poll $poll): Answer
    {
        $this->poll = $poll;
        return $this;
    }

    public static function getFormTypes()
    {
        return [
            self::TYPE_TEXT => TextAnswerType::class,
            self::TYPE_NUMERIC => NumericType::class,
            self::TYPE_SINGLE_OPTION => SingleOptionType::class,
            self::TYPE_MULTIPLE_OPTION => MultipleOptionType::class,
            self::TYPE_RANGE => RangeType::class,
            self::TYPE_FILE => FileType::class
        ];
    }

    /**
     * @inheritdoc
     */
    public function getAnsweredValue()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function setAnsweredValue($value)
    {
        return $this;
    }

    public function __clone()
    {
        $this->id = null;

        return $this;
    }
}
