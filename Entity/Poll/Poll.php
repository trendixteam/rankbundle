<?php


namespace Trendix\RankBundle\Entity\Poll;

use Doctrine\Common\Collections\ArrayCollection;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\Utility\Dates;
use Trendix\RankBundle\Entity\Answer\Answer;
use Trendix\RankBundle\Entity\Rank;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Entity\SupplierClassification;
use Trendix\RankBundle\Entity\Versions\Version;
use Trendix\AdminBundle\EventListener\DiscriminatorEntry;

/**
 * Rank
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Trendix\RankBundle\Entity\Repository\PollRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name = "type", type = "string")
 * @DiscriminatorEntry(value = "poll")
 */
class Poll
{
    const TYPE_EVALUATIONPOLL = 'EVALUATIONPOLL';
    const TYPE_RECURRENTPOLL = 'RECURRENTPOLL';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Rank
     *
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Rank", inversedBy="polls")
     */
    private $rank;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Trendix\AdminBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Trendix\RankBundle\Entity\Answer\Answer", mappedBy="poll", cascade={"persist"})
     */
    private $answers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    public function __construct()
    {
        $this->created_at = Dates::getDateTimeWithMilisecs();
        $this->updated_at = Dates::getDateTimeWithMilisecs();
        $this->answers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Rank
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param Rank $rank
     * @return Poll
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Poll
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param Answer $answers
     * @return Poll
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
        return $this;
    }

    /**
     * @param Answer $answer
     * @return $this
     */
    public function addAnswer($answer)
    {
        $answer->setPoll($this);
        $this->answers->add($answer);
        return $this;
    }

    /**
     * @param Answer $answer
     * @return $this
     */
    public function removeAnswer($answer)
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     * @return Poll
     */
    public function setCreatedAt( $created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param \DateTime $updated_at
     * @return Poll
     */
    public function setUpdatedAt( $updated_at)
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    public static function getClassByType($type = '')
    {
        $types = [
            self::TYPE_EVALUATIONPOLL => EvaluationPoll::class,
            self::TYPE_RECURRENTPOLL => RecurrentPoll::class
        ];

        if (!isset($types[$type])) {
            return null;
        }

        return $types[$type];
    }
}