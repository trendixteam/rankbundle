<?php

namespace Trendix\RankBundle\Entity\Criterion;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Classes\Criterion\CriterionAbstractType;
use Trendix\RankBundle\Entity\Answer\MultipleOptionAnswer;
use Trendix\RankBundle\Ranking\MultipleOptionRanking;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class MultipleOptionCriterion extends CriterionAbstractType
{
    const CRITERION_TYPE_NAME = 'evaluations.criterion.multioption';

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Trendix\RankBundle\Entity\Criterion\Option", mappedBy="multipleOptionCriterion", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $options;

    public function __construct()
    {
        $this->options = new ArrayCollection();
        $this->options->add(new Option());
    }

    /**
     * @return ArrayCollection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param ArrayCollection $options
     * @return self
     */
    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function addOption(Option $option)
    {
        if (!$this->options->contains($option)) {
            $option->setMultipleOptionCriterion($this);
            $this->options->add($option);
        }
        return $this;
    }

    public function removeOption(Option $option)
    {
        if ($this->options->contains($option)) {
            $option->setMultipleOptionCriterion(null);
            $this->options->removeElement($option);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public static function getName()
    {
        return self::CRITERION_TYPE_NAME;
    }

    /**
     * @return mixed
     */
    public static function getAnswerClass()
    {
        return MultipleOptionAnswer::class;
    }

    public static function getAnswerFormType()
    {
        $answerClass = self::getAnswerClass();
        return $answerClass::getFormTypeClass();
    }

    /**
     * @return mixed
     */
    public function getFormulaClass()
    {
        return MultipleOptionRanking::class;
    }

    /**
     * Si es múltiple el peso es la suma del peso de todas las opciones si no, el de la opción con más peso
     * @return int
     */
    public function getWeight()
    {
        $weight = 0;
        foreach ($this->options as $opt) {
            $weight += $opt->getWeight();
        }

        return $weight;
    }

    public function getType()
    {
        return self::TYPE_MULTIPLE_OPTION;
    }

    public function __clone() {
        // Clonamos las opciones
        $newOptions = new ArrayCollection();
        if (null !== $this->options) {
            foreach ($this->options as $option) {
                $newOption = clone $option;
                $newOption->setMultipleOptionCriterion($this);
                $newOptions->add($newOption);
            }
        }
        $this->options = $newOptions;
    }

    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();
        $result['criterionType'] = 'multiple_options';

        $options = [];
        foreach ($this->options as $option) {
            $options[] = $option->jsonSerialize();
        }
        $result['options'] = $options;
        return $result;
    }
}