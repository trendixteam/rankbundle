<?php

namespace Trendix\RankBundle\Entity\Criterion;

use Trendix\RankBundle\Classes\Criterion\CriterionAbstractType;
use Trendix\RankBundle\Classes\Criterion\DecimalsPropertyTrait;
use Trendix\RankBundle\Entity\Answer\NumericAnswer;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Ranking\NumericRanking;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class NumericCriterion extends CriterionAbstractType
{
    const CRITERION_TYPE_NAME = 'evaluations.criterion.number';

    const CRITERION_SUBTYPE_HIGHER = 0;
    const CRITERION_SUBTYPE_LOWER = 1;

    /**
     * @var int
     * @ORM\Column(name="subtype", type="integer")
     */
    private $subtype;

    /**
     * @var float
     * @ORM\Column(name="weight", type="float")
     */
    private $weight;

    /**
     * @var float
     * @ORM\Column(name="min_value", type="float")
     */
    private $min_value;

    /**
     * @var float
     * @ORM\Column(name="max_value", type="float")
     */
    private $max_value;

    /**
     * NumericCriterion constructor.
     */
    public function __construct()
    {
        $this->min_value = 0;
        $this->max_value = 100;
        $this->decimals = 2;
    }

    use DecimalsPropertyTrait;

    public static function getName()
    {
        return self::CRITERION_TYPE_NAME;
    }

    public function getType()
    {
        return self::TYPE_NUMERIC;
    }

    /**
     * @return string
     */
    public static function getAnswerClass()
    {
        return NumericAnswer::class;
    }

    public static function getAnswerFormType()
    {
        $answerClass = self::getAnswerClass();
        return $answerClass::getFormTypeClass();
    }

    /**
     * @return string
     */
    public function getFormulaClass()
    {
        return NumericRanking::class;
    }

    /**
     * @return int
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * @param int $subtype
     * @return NumericCriterion
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return NumericCriterion
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return float
     */
    public function getMinValue()
    {
        return $this->min_value;
    }

    /**
     * @param float $min_value
     * @return NumericCriterion
     */
    public function setMinValue($min_value): NumericCriterion
    {
        $this->min_value = $min_value;
        return $this;
    }

    /**
     * @return float
     */
    public function getMaxValue()
    {
        return $this->max_value;
    }

    /**
     * @param float $max_value
     * @return NumericCriterion
     */
    public function setMaxValue($max_value): NumericCriterion
    {
        $this->max_value = $max_value;
        return $this;
    }

    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();
        $result['criterionType'] = 'numeric';
        $result['subtype'] = $this->subtype;
        $result['min_value'] = $this->min_value;
        $result['max_value'] = $this->max_value;
        return $result;
    }
}