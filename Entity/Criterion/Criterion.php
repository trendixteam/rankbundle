<?php

namespace Trendix\RankBundle\Entity\Criterion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Trendix\AdminBundle\Entity\User;
use Trendix\AdminBundle\Utility\Numeric;
use Trendix\RankBundle\Entity\Section;
use Trendix\RankBundle\Form\Criterion\FileType;
use Trendix\RankBundle\Form\Criterion\MultipleOptionType;
use Trendix\RankBundle\Form\Criterion\NumericType;
use Trendix\RankBundle\Form\Criterion\RangeType;
use Trendix\RankBundle\Form\Criterion\SingleOptionType;
use Trendix\RankBundle\Form\Criterion\TextCriterionType;

/**
 * Criterion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Trendix\RankBundle\Entity\Repository\CriterionRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name = "type", type = "string")
 * @ORM\DiscriminatorMap({
 *     "numeric" = "Trendix\RankBundle\Entity\Criterion\NumericCriterion",
 *     "range" = "Trendix\RankBundle\Entity\Criterion\RangeCriterion",
 *     "single_option" = "Trendix\RankBundle\Entity\Criterion\SingleOptionCriterion",
 *     "multiple_options" = "Trendix\RankBundle\Entity\Criterion\MultipleOptionCriterion",
 *     "text" = "Trendix\RankBundle\Entity\Criterion\TextCriterion",
 *     "file" = "Trendix\RankBundle\Entity\Criterion\FileCriterion"
 * })
 */
abstract class Criterion implements \JsonSerializable
{
    const TYPE_NUMERIC = 'numeric';
    const TYPE_RANGE = 'range';
    const TYPE_SINGLE_OPTION = 'single_option';
    const TYPE_MULTIPLE_OPTION = 'multiple_options';
    const TYPE_TEXT = 'text';
    const TYPE_FILE = 'file';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Criterion
     * @ORM\OneToOne(targetEntity="Trendix\RankBundle\Entity\Criterion\Criterion", cascade={"persist"})
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var Section
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Section", inversedBy="criteria", cascade={"persist"})
     */
    private $section;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Trendix\RankBundle\Entity\Answer\Answer", mappedBy="criterion", cascade={"persist"})
     */
    private $answers;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Criterion
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Criterion $parent
     * @return Criterion
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Criterion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return Criterion
     */
    public function setPosition(int $position): Criterion
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Criterion
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param Section $section
     * @return Criterion
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param ArrayCollection $answers
     * @return Criterion
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
        return $this;
    }

    public function addAnswer($answer)
    {
        $this->answers->add($answer);
    }

    public function removeAnswer($answer)
    {
        if ($this->answers->contains($answer)) {
            $this->answers->removeElement($answer);
        }
    }

    /**
     * Devuelve el Alias de la pregunta según la posición de la sección y la propia pregunta.
     * Sólo debe usarse para identificar las respuestas. Nunca en el diseñador
     * @return string
     */
    public function getNumerationAlias()
    {
        return $this->getSection()->getNumerationAlias().$this->getPosition();
    }

    public static function getTypes()
    {
        return [
            self::TYPE_NUMERIC => NumericCriterion::CRITERION_TYPE_NAME,
            self::TYPE_SINGLE_OPTION => SingleOptionCriterion::CRITERION_TYPE_NAME,
            self::TYPE_MULTIPLE_OPTION => MultipleOptionCriterion::CRITERION_TYPE_NAME,
            self::TYPE_RANGE => RangeCriterion::CRITERION_TYPE_NAME,
            self::TYPE_TEXT => TextCriterion::CRITERION_TYPE_NAME,
            self::TYPE_FILE => FileCriterion::CRITERION_TYPE_NAME
        ];
    }

    public static function getTypesClass()
    {
        return [
            self::TYPE_NUMERIC => NumericCriterion::class,
            self::TYPE_SINGLE_OPTION => SingleOptionCriterion::class,
            self::TYPE_MULTIPLE_OPTION => MultipleOptionCriterion::class,
            self::TYPE_RANGE => RangeCriterion::class,
            self::TYPE_TEXT => TextCriterion::class,
            self::TYPE_FILE => FileCriterion::class
        ];
    }

    public static function getFormTypes()
    {
        return [
            self::TYPE_NUMERIC => NumericType::class,
            self::TYPE_SINGLE_OPTION => SingleOptionType::class,
            self::TYPE_MULTIPLE_OPTION => MultipleOptionType::class,
            self::TYPE_RANGE => RangeType::class,
            self::TYPE_TEXT => TextCriterionType::class,
            self::TYPE_FILE => FileType::class
        ];
    }

    public static function getClassNameFromAlias($alias)
    {
        return self::getTypesClass()[$alias];
    }

    public function __toString()
    {
        return $this->title;
    }

    public function __clone() {
        if ($this->id) {
            // Eliminamos el id. En este caso no es necesario clonar las respuestas.
            $this->id = null;
            $this->answers = new ArrayCollection();
        }
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'position' => $this->position,
            'title' => $this->title,
            'description' => $this->description
        ];
    }
}
