<?php

namespace Trendix\RankBundle\Entity\Criterion;

use Trendix\RankBundle\Classes\Criterion\CriterionAbstractType;
use Trendix\RankBundle\Entity\Answer\FileAnswer;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Ranking\VoidRanking;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class FileCriterion extends CriterionAbstractType
{
    const CRITERION_TYPE_NAME = 'evaluations.criterion.file';
    const DEFAULT_LIMIT_FILE = 1;
    const MAX_LIMIT_FILE = 10;

    /**
     * @var integer
     * @ORM\Column(name="limit_files", type="integer")
     */
    private $limitFiles;

    public function __construct()
    {
        parent::__construct();
        $this->limitFiles = self::DEFAULT_LIMIT_FILE;
    }

    /**
     * @return int
     */
    public function getLimitFiles()
    {
        return $this->limitFiles;
    }

    /**
     * @param int $limitFiles
     *
     * @return FileCriterion
     */
    public function setLimitFiles($limitFiles)
    {
        $this->limitFiles = $limitFiles;
        return $this;
    }

    public static function getName()
    {
        return self::CRITERION_TYPE_NAME;
    }

    /**
     * @return string
     */
    public static function getAnswerClass()
    {
        return FileAnswer::class;
    }

    public static function getAnswerFormType()
    {
        $answerClass = self::getAnswerClass();
        return $answerClass::getFormTypeClass();
    }

    /**
     * @return string
     */
    public function getFormulaClass()
    {
        return VoidRanking::class;
    }

    public function getType()
    {
        return self::TYPE_FILE;
    }

    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();
        $result['criterionType'] = 'file';
        $result['limitFiles'] = $this->limitFiles;
        return $result;
    }
}