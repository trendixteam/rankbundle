<?php

namespace Trendix\RankBundle\Entity\Criterion;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Classes\Criterion\CriterionAbstractType;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class Option implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title")
     */
    private $title;

    /**
     * @var float
     * @ORM\Column(name="weight")
     */
    private $weight;

    /**
     * @var Criterion
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Criterion\SingleOptionCriterion", inversedBy="options")
     */
    private $singleOptionCriterion;

    /**
     * @var Criterion
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Criterion\MultipleOptionCriterion", inversedBy="options")
     */
    private $multipleOptionCriterion;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Option
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return Option
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return Criterion
     */
    public function getSingleOptionCriterion(): Criterion
    {
        return $this->singleOptionCriterion;
    }

    /**
     * @param Criterion $singleOptionCriterion
     * @return Option
     */
    public function setSingleOptionCriterion($singleOptionCriterion)
    {
        $this->singleOptionCriterion = $singleOptionCriterion;
        return $this;
    }

    /**
     * @return Criterion
     */
    public function getMultipleOptionCriterion(): Criterion
    {
        return $this->multipleOptionCriterion;
    }

    /**
     * @param Criterion $multipleOptionCriterion
     * @return Option
     */
    public function setMultipleOptionCriterion($multipleOptionCriterion)
    {
        $this->multipleOptionCriterion = $multipleOptionCriterion;
        return $this;
    }

    /**
     * @return CriterionTypeInterface
     */
    public function getCriterion()
    {
        if ($this->singleOptionCriterion instanceof SingleOptionCriterion) {
            return $this->singleOptionCriterion;
        }

        return $this->multipleOptionCriterion;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'weight' => $this->weight
        ];
    }


}