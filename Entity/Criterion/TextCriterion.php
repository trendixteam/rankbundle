<?php

namespace Trendix\RankBundle\Entity\Criterion;

use Trendix\RankBundle\Classes\Criterion\CriterionAbstractType;
use Trendix\RankBundle\Entity\Answer\NumericAnswer;
use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Entity\Answer\TextAnswer;
use Trendix\RankBundle\Ranking\VoidRanking;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class TextCriterion extends CriterionAbstractType
{
    const CRITERION_TYPE_NAME = 'evaluations.criterion.text';
    const DEFAULT_LIMIT_CHARS = 300;

    /**
     * @var integer
     * @ORM\Column(name="limit_chars", type="integer")
     */
    private $limitChars;

    public function __construct()
    {
        $this->limitChars = self::DEFAULT_LIMIT_CHARS;
    }

    /**
     * @return int
     */
    public function getLimitChars()
    {
        return $this->limitChars;
    }

    /**
     * @param int $limitChars
     * @return TextCriterion
     */
    public function setLimitChars($limitChars)
    {
        $this->limitChars = $limitChars;
        return $this;
    }

    public static function getName()
    {
        return self::CRITERION_TYPE_NAME;
    }

    /**
     * @return string
     */
    public static function getAnswerClass()
    {
        return TextAnswer::class;
    }

    public static function getAnswerFormType()
    {
        $answerClass = self::getAnswerClass();
        return $answerClass::getFormTypeClass();
    }

    /**
     * @return string
     */
    public function getFormulaClass()
    {
        return VoidRanking::class;
    }

    public function getType()
    {
        return self::TYPE_TEXT;
    }

    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();
        $result['criterionType'] = 'text';
        $result['limitChars'] = $this->limitChars;
        return $result;
    }
}