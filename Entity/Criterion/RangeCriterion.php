<?php

namespace Trendix\RankBundle\Entity\Criterion;

use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Classes\Criterion\CriterionAbstractType;
use Trendix\RankBundle\Classes\Criterion\DecimalsPropertyTrait;
use Trendix\RankBundle\Entity\Answer\RangeAnswer;
use Trendix\RankBundle\Ranking\RangeRanking;

/**
 * @ORM\Table()
 * @ORM\Entity()
 */
class RangeCriterion extends CriterionAbstractType
{
    const CRITERION_TYPE_NAME = 'evaluations.criterion.range';

    /**
     * @var float
     * @ORM\Column(name="min_value")
     */
    private $min_value;

    /**
     * @var float
     * @ORM\Column(name="max_value")
     */
    private $max_value;

    /**
     * @var float
     * @ORM\Column(name="min_weight")
     */
    private $min_weight;

    /**
     * @var float
     * @ORM\Column(name="max_weight")
     */
    private $max_weight;

    use DecimalsPropertyTrait;

    /**
     * @return float
     */
    public function getMinValue()
    {
        return $this->min_value;
    }

    /**
     * @param float $min_value
     */
    public function setMinValue($min_value)
    {
        $this->min_value = $min_value;
    }

    /**
     * @return float
     */
    public function getMaxValue()
    {
        return $this->max_value;
    }

    /**
     * @param float $max_value
     */
    public function setMaxValue($max_value)
    {
        $this->max_value = $max_value;
    }

    /**
     * @return float
     */
    public function getMinWeight()
    {
        return $this->min_weight;
    }

    /**
     * @param float $min_weight
     */
    public function setMinWeight($min_weight)
    {
        $this->min_weight = $min_weight;
    }

    /**
     * @return float
     */
    public function getMaxWeight()
    {
        return $this->max_weight;
    }

    /**
     * @param float $max_weight
     */
    public function setMaxWeight($max_weight)
    {
        $this->max_weight = $max_weight;
    }

    /**
     * @return mixed
     */
    public static function getName()
    {
        return self::CRITERION_TYPE_NAME;
    }

    /**
     * @return mixed
     */
    public static function getAnswerClass()
    {
        return RangeAnswer::class;
    }

    public static function getAnswerFormType()
    {
        $answerClass = self::getAnswerClass();
        return $answerClass::getFormTypeClass();
    }

    /**
     * @return mixed
     */
    public function getFormulaClass()
    {
        return RangeRanking::class;
    }

    /**
     * Devuelve el peso máximo del criterio
     * @return float
     */
    public function getWeight()
    {
        if ($this->min_weight > $this->max_weight) {
            return $this->min_weight;
        }

        return $this->max_weight;
    }

    public function getType()
    {
        return self::TYPE_RANGE;
    }

    public function jsonSerialize()
    {
        $result = parent::jsonSerialize();
        $result['criterionType'] = 'range';
        $result['min_value'] = $this->min_value;
        $result['max_value'] = $this->max_value;
        return $result;
    }
}