<?php

namespace Trendix\RankBundle\Entity\Versions;


use Doctrine\ORM\Mapping as ORM;
use Trendix\RankBundle\Entity\Section;
// * @ORM\Entity()
/*
 * Class SectionVersion
 * @package Trendix\RankBundle\Entity\Versions
 */
class SectionVersion
{
    /*
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /*
     * @var Version
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Versions\Version", inversedBy="sections")
     */
    private $version;

    /*
     * @var float
     * @ORM\Column(name="weight", type="float")
     */
    private $weight;

    /*
     * @var boolean
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /*
     * @var Section
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Section", inversedBy="sectionVersions", cascade={"persist"})
     */
    private $section;

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @return Version
     */
    public function getVersion()
    {
        return $this->version;
    }

    /*
     * @param Version $version
     * @return SectionVersion
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /*
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /*
     * @param float $weight
     * @return SectionVersion
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /*
     * @return bool
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /*
     * @param bool $visible
     * @return SectionVersion
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

    /*
     * @return Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /*
     * @param Section $section
     * @return SectionVersion
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    public function __clone()
    {
        if ($this->id) {
            // Eliminamos el id. En este caso no es necesario clonar las respuestas.
            $this->id = null;
        }
    }
}