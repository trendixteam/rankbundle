<?php

namespace Trendix\RankBundle\Entity\Versions;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Trendix\AdminBundle\Entity\User;
use Trendix\DirectorioBundle\Entity\Subcategoria;
use Trendix\RankBundle\Classes\Criterion\CriterionTypeInterface;
use Trendix\RankBundle\Classes\Versions\VersionableInterface;
use Trendix\RankBundle\Entity\Evaluation\EvaluationPoll;
use Trendix\RankBundle\Entity\EvaluationForm;
use Trendix\RankBundle\Entity\Section;
use Trendix\RankBundle\Entity\SupplierClassification;
// * @ORM\Entity(repositoryClass="Trendix\RankBundle\Entity\Repository\VersionRepository")
/*
 * Class Version
 * @package Trendix\RankBundle\Entity\Versions
 */
class Version
{
    /*
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /*
     * @var \DateTime
     * @ORM\Column(name="publish_at", type="datetime", nullable=true)
     */
    private $publish_at;

    /*
     * @var User
     * @ORM\ManyToOne(targetEntity="Trendix\AdminBundle\Entity\User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /*
     * @var $evaluationForm
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\EvaluationForm", inversedBy="versions")
     */
    private $evaluationForm;

    /*
     * Las versiones fusionadas se pueden asociar a una EvaluationPoll cerrada
     * para el calculo de su ranking
     *
     * @var EvaluationPoll
     * @ORM\OneToOne(targetEntity="Trendix\RankBundle\Entity\Evaluation\EvaluationPoll", inversedBy="version")
     */
    private $evaluationPoll;

    /*
     * @var Subcategoria
     * @ORM\ManyToOne(targetEntity="Trendix\DirectorioBundle\Entity\Subcategoria", inversedBy="evaluationVersions")
     */
    private $subcategoria;

    /*
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Trendix\RankBundle\Entity\Versions\SectionVersion", mappedBy="version", cascade={"all"})
     */
    private $sections;

    /*
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Trendix\RankBundle\Entity\Versions\CriterionVersion", mappedBy="version", cascade={"all"})
     */
    private $criteria;

    /*
     * @var float
     * @ORM\Column(name="classification_weight", type="float")
     */
    private $classificationWeight;


    public function __construct(VersionableInterface $form = null)
    {
        $this->sections = new ArrayCollection();
        $this->criteria = new ArrayCollection();

        if (null !== $form) {
            if ($form instanceof EvaluationForm) {
                $this->evaluationForm = $form;
            }

            $this->generateSectionsVersion();
            $this->generateCriteriaVersion();
        }
    }

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @return \DateTime
     */
    public function getPublishAt()
    {
        return $this->publish_at;
    }

    /*
     * @param \DateTime $publish_at
     * @return Version
     */
    public function setPublishAt($publish_at)
    {
        $this->publish_at = $publish_at;
        return $this;
    }

    /*
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /*
     * @param User $author
     * @return Version
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
        return $this;
    }

    /*
     * @return mixed
     */
    public function getEvaluationForm()
    {
        return $this->evaluationForm;
    }

    /*
     * @param mixed $evaluationForm
     * @return Version
     */
    public function setEvaluationForm($evaluationForm)
    {
        $this->evaluationForm = $evaluationForm;
        return $this;
    }

    /*
     * @return EvaluationPoll
     */
    public function getEvaluationPoll()
    {
        return $this->evaluationPoll;
    }

    /*
     * @param EvaluationPoll $evaluationPoll
     * @return Version
     */
    public function setEvaluationPoll(EvaluationPoll $evaluationPoll)
    {
        $this->evaluationPoll = $evaluationPoll;
        return $this;
    }

    /*
     * @return Subcategoria
     */
    public function getSubcategoria()
    {
        return $this->subcategoria;
    }

    /*
     * @param Subcategoria $subcategoria
     * @return Version
     */
    public function setSubcategoria(Subcategoria $subcategoria)
    {
        $this->subcategoria = $subcategoria;
        return $this;
    }

    /*
     * @return ArrayCollection
     */
    public function getSections()
    {
        return $this->sections;
    }

    /*
     * @param ArrayCollection $sections
     * @return Version
     */
    public function setSections(ArrayCollection $sections)
    {
        $this->sections = $sections;
        return $this;
    }

    /*
     * @param SectionVersion $sv
     * @return $this
     */
    public function addSection(SectionVersion $sv)
    {
        $sv->setVersion($this);
        $this->sections->add($sv);
        return $this;
    }

    /*
     * @param SectionVersion $sv
     * @return $this
     */
    public function removeSection(SectionVersion $sv)
    {
        if ($this->sections->contains($sv)) {
            $this->sections->removeElement($sv);
        }

        return $this;
    }

    /*
     * @return ArrayCollection
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /*
     * @param ArrayCollection $criteria
     * @return Version
     */
    public function setCriteria(ArrayCollection $criteria)
    {
        $this->criteria = $criteria;
        return $this;
    }

    /*
     * @param CriterionVersion $cv
     * @return $this
     */
    public function addCriterion(CriterionVersion $cv)
    {
        $cv->setVersion($this);
        $this->criteria->add($cv);
        return $this;
    }

    /*
     * @param CriterionVersion $cv
     * @return $this
     */
    public function removeCriterion(CriterionVersion $cv)
    {
        if ($this->criteria->contains($cv)) {
            $this->criteria->removeElement($cv);
        }

        return $this;
    }

    /*
     * @return float
     */
    public function getClassificationWeight()
    {
        return $this->classificationWeight;
    }

    /*
     * @param float $classificationWeight
     * @return Version
     */
    public function setClassificationWeight(float $classificationWeight)
    {
        $this->classificationWeight = $classificationWeight;
        return $this;
    }

    /*
     * Devuelve la versión de de la sección pasada por parámetro
     * @param Section $section
     * @return mixed|null
     */
    public function findBySection(Section $section)
    {
        if ($section->getRank() instanceof SupplierClassification) {
            $sv = new SectionVersion();
            $sv->setSection($section);
            $sv->setWeight($this->getClassificationWeight());
            return $sv;
        }

        foreach ($this->sections as $s) {
            if ($s->getSection()->getId() == $section->getId()) {
                return $s;
            }
        }

        return null;
    }

    /*
     * Devuelve la versión de del criterio pasado por parámetro
     * @param CriterionTypeInterface $criterion
     * @return mixed|null
     */
    public function findByCriterion(CriterionTypeInterface $criterion)
    {
        foreach ($this->criteria as $c) {
            if ($c->getCriterion()->getId() == $criterion->getId()) {
                return $c;
            }
        }

        return null;
    }

    /*
     * @param Section $section
     * @return null
     */
    public function getSectionWeight(Section $section)
    {
        foreach ($this->sections as $s) {
            if ($s->getSection()->getId() == $section->getId()) {
                return $s->getWeight();
            }
        }

        return null;
    }

    /*
     * @param CriterionTypeInterface $criterion
     * @return bool
     */
    public function isCriterionVisible(CriterionTypeInterface $criterion)
    {
        if ($criterion->getSection()->getRank() instanceof SupplierClassification) {
            return true;
        }

        foreach ($this->criteria as $c) {
            if ($c->getCriterion()->getId() == $criterion->getId()) {
                return $c->isVisible();
            }
        }

        return false;
    }

    /*
     * @param Section $section
     * @return bool
     */
    public function isSectionVisible(Section $section)
    {
        foreach ($section->getCriteria() as $c) {
            if ($this->isCriterionVisible($c)) {
                return true;
            }
        }

        return false;
    }

    /*
     * Genera las versiones de las secciones según la estructura del formulario
     */
    private function generateSectionsVersion()
    {
        foreach ($this->evaluationForm->getSections() as $section) {
            $sectionVersion = new SectionVersion();
            $sectionVersion->setVersion($this);
            $sectionVersion->setVisible(true);
            $sectionVersion->setSection($section);
            $sectionVersion->setWeight($section->getWeight());

            $this->sections->add($sectionVersion);
        }
    }

    /*
     * Genera las versiones de los criterios según la estructura del formulario
     */
    private function generateCriteriaVersion()
    {
        foreach ($this->evaluationForm->getSections() as $section) {
            foreach ($section->getCriteria() as $crierion) {
                $criterionVersion = new CriterionVersion();
                $criterionVersion->setVersion($this);
                $criterionVersion->setVisible(true);
                $criterionVersion->setCriterion($crierion);

                $this->criteria->add($criterionVersion);
            }
        }
    }

    public function __clone()
    {
        if ($this->id) {
            // Eliminamos el id para que se genere una nueva fila en base de datos
            $this->id = null;
            $this->publish_at = new \DateTime();

            // clonamos las secciones hijas
            $newSections = new ArrayCollection();
            foreach ($this->sections as $section) {
                $newSection = clone $section;
                $newSection->setVersion($this);
                $newSections->add($newSection);
            }
            $this->sections = $newSections;

            // Clonamos los criterios hijos
            $newCriteria = new ArrayCollection();
            foreach ($this->criteria as $criterion) {
                $newCriterion = clone $criterion;
                $newCriterion->setVersion($this);
                $newCriteria->add($newCriterion);
            }
            $this->criteria = $newCriteria;
        }
    }
}