<?php

namespace Trendix\RankBundle\Entity\Versions;


use Trendix\RankBundle\Entity\Criterion\Criterion;
use Doctrine\ORM\Mapping as ORM;
// * @ORM\Entity()

/*
 * Class CriterionVersion
 * @package Trendix\RankBundle\Entity\Versions
 */
class CriterionVersion
{
    /*
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /*
     * @var Version
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Versions\Version", inversedBy="criteria")
     */
    private $version;

    /*
     * @var boolean
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /*
     * @var Criterion
     * @ORM\ManyToOne(targetEntity="Trendix\RankBundle\Entity\Criterion\Criterion", inversedBy="criterionVersions")
     */
    private $criterion;

    /*
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * @return Version
     */
    public function getVersion()
    {
        return $this->version;
    }

    /*
     * @param Version $version
     * @return CriterionVersion
     */
    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    /*
     * @return bool
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /*
     * @param bool $visible
     * @return SectionVersion
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

    /*
     * @return Criterion
     */
    public function getCriterion()
    {
        return $this->criterion;
    }

    /*
     * @param Criterion $criterion
     * @return CriterionVersion
     */
    public function setCriterion($criterion)
    {
        $this->criterion = $criterion;
        return $this;
    }

    public function __clone()
    {
        if ($this->id) {
            // Eliminamos el id. En este caso no es necesario clonar las respuestas.
            $this->id = null;
        }
    }
}