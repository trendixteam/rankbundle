<?php

namespace Trendix\RankBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Trendix\AdminBundle\Utility\Debug;
use Trendix\RankBundle\Entity\Evaluation\EvaluationPoll;
use Trendix\RankBundle\Services\Ranking;
use Trendix\SubastasBundle\Entity\Proveedor;

class SupplierValorationListener
{
    /**
     * @var Ranking
     */
    private $ranking;

    public function __construct(Ranking $ranking)
    {
        $this->ranking = $ranking;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof EvaluationPoll) {
            return;
        }

        $em = $args->getEntityManager();

        if ($entity->getExpiresAt() instanceof \DateTime) {
            $proveedor = $entity->getUser()->getProveedor();
            if ($proveedor instanceof Proveedor) {
                $ranking = $this->ranking->getArrayRankingFromPoll($entity);
                $proveedor->setValoracion($ranking['userPercent']);
                $em->persist($proveedor);
                $em->flush();
            }
        }
    }
}