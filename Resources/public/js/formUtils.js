/*
 TODO: Casos a revisar
    - El primer bloque, envía todos sus criterios con el name __block__, cuando debería ser 0, en el espacio del bloque
    - En el último bloque sin criterios o en un bloque que no tenga criterios,se queda la plantilla con name __criterion__ y los valores por defecto por algún lado.
 */
function action_addFormElementCollection($action_button)
{
    var isValid = true;
    var widgetContainer = false;
    if (($action_button.data('widget-container') != undefined)) {
        widgetContainer = true;
    }
    var prototype_name = '__name__';
    if ($action_button.data('prototype-name') != undefined) {
        prototype_name = $action_button.data('prototype-name');
    }
    console.log(prototype_name);
    var reg = new RegExp(prototype_name, "g");

    // Si está definida la propiedad data-validate se ejecuta su funcion referenciada
    // Devolverá obligatoriamente un boleano
    if ($action_button.data('validate') != undefined) {
        var validate = $action_button.data('validate');
        isValid = window[validate]();
    }

    if (!isValid) {
        return false;
    }

    if (widgetContainer) {
        var formWidgetcontainer = $action_button.closest('.formWidgetContainer');
        var prototype = formWidgetcontainer.find('#' + $action_button.data('prototype')).html();
        var container = formWidgetcontainer.find('#' + $action_button.data('container'));
    } else {
        var prototype = $('#' + $action_button.data('prototype')).html();
        var container = $('#' + $action_button.data('container'));
    }

    var index = container.data('index');
    index++;
    container.data('index', index);

    // Creamos el nuevo subform
    var newForm = prototype.replace(reg, index);

    // Si el form es de un bloque, tenemos que modificar la id de la lista de criterios:
    if(prototype_name == '__block__') {
        var criteriaIndex = $('.lista_criterios').length - 2;
        criteriaIndex++;
        console.log(criteriaIndex);
        newForm = newForm.replace('lista_criterios_0', 'lista_criterios_' + criteriaIndex);
        console.log(newForm);
        var criteriaReg = new RegExp('data-block="0"', "g");
        newForm = newForm.replace(criteriaReg, 'data-block="' + criteriaIndex + '"');
    } else if(prototype_name == '__criterion__') {// Si el form es un criterio, tenemos que modificar el data-block
        var block = $action_button.data('block');
        newForm = newForm.replace('data-block=""', 'data-block="' + block + '"');
    }

    container.append(newForm);
    $('#' + $(newForm).attr('id')).css('display', 'none').show(400);

    if ($action_button.data('callback') != undefined) {
        var id_element = $(newForm).attr('id');
        var callback = $action_button.data('callback');
        setTimeout(function() {
            window[callback](id_element);
        }, 1);
    }
}

function action_removeFormElementCollection($action_button)
{
    if ($action_button.data('widget-container') != undefined) {
        var formWidgetcontainer = $action_button.closest('.formWidgetContainer');
        var element = formWidgetcontainer.find('#' + $action_button.data('id'));
    } else {
        var element = $('#'+ $action_button.data('id'));
    }

    element.hide(400, function() {
        $(this).remove();
        if ($action_button.data('callback') != undefined) {
            var id_element = $action_button.data('id');
            var callback = $action_button.data('callback');
            setTimeout(function() {
                window[callback](id_element);
            }, 1);
        }
    });
}

function action_saveCriterion($action_button)
{
    var modal = $action_button.closest('.modal');
    var criterionId = modal.attr('id');
    var container = $('#' + modal.data('container'));
    var index = container.data('index');
    modal.modal('hide');
    var title = modal.find('.criterion-title').val();
    var type = modal.find('.tipoCriterio').val();
    var weight = modal.find('.criterion-weight').val();
    console.log($action_button.data('block'));
    var table = $('#lista_criterios_' + $action_button.data('block'));
    if(modal.hasClass('first-time')) {
        var row =
            '        <tr data-criterion="#' + criterionId + '">' +
            '            <td></td>' +
            '            <td class="title">' + title + '</td>' +
            '            <td class="type">' + type + '</td>' +
            '            <td class="weight" style="text-align: center;">' + weight + '</td>' +
            '            <td style="text-align: center;">' +
            '               <a href="#" class="btn btn-default" data-action="editCriterion" data-target="#' + criterionId + '"><i class="zmdi zmdi-edit"></i></a>' +
            '               <a href="#" class="btn btn-default" data-action="removeCriterion" data-target="#' + criterionId + '"><i class="zmdi zmdi-delete"></i></a>' +
            '           </td>' +
            '        </tr>';
        table.find('tbody').append(row);
    } else {
        var row = table.find('tr[data-criterion="#' + criterionId + '"]');
        row.find('td.title').text(title);
        row.find('td.type').text(type);
        row.find('td.weight').text(weight);
    }

    modal.removeClass('first-time');
}

function action_undoCriterionChanges($action_button)
{
    var modal = $action_button.closest('.modal');
    var container = $('#' + modal.data('container'));
    var index = container.data('index');
    if(modal.hasClass('first-time')) {
        index--;
        container.data('index', index);
        modal.modal('hide');
        setTimeout(function() {
            modal.remove();
        }, 1000);
    } else {
        modal.find('input, textarea, select').each(function() {
            $(this).val($(this).data('value'));
        });
        modal.modal('hide');
    }
}


function action_removeCriterion($action_button)
{
    var row = $action_button.closest('tr');
    var modal = $($action_button.data('target'));
    var container = $('#' + modal.data('container'));
    var index = container.data('index');
    index--;
    container.data('index', index);
    modal.remove();
    row.remove();
}

function showModalCriterion(id)
{
    $(id).modal({
        backdrop: 'static',
        keyboard: false
    });
}

// systemmessages.js
var systemmessage_counter = 0;

function showMessage(tipo, message, time)
{
    if (time == undefined) {
        time = 7000;
    }
    if (existsMessage(tipo, message)) {
        return false;
    }
    blockmessage = '<div class="systemmessage alert ' + tipo + ' num' + systemmessage_counter + ' hide"> ' +
        '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '<div class="systemmessagecontent">' + message + '</div>' +
        '</div>';
    $(blockmessage).prependTo('#system-message-block');
    $('.num'+ systemmessage_counter).show('normal');
    hideMessage('num' + systemmessage_counter, time);
    systemmessage_counter++;
}

function hideMessage(name, time)
{
    setTimeout(function(){
        $('.' + name).hide('normal');
    }, time);
}

function existsMessage(tipo, message)
{
    existe = false;
    $(".systemmessage").each(function() {
        if ($(this).css('display') != 'none') {
            if ($(this).find('.systemmessagecontent').html() == message && $(this).hasClass(tipo)) {
                existe = true;
            }
        }
    });

    return existe;
}